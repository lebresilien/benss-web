<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\Localisation;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('roles')->delete();

        $roles = [
            'medecin',
            'patient',
            'infirmier',
            'user'
         ];

        foreach($roles as $role) {

            Role::create(['name' => $role]);

        }
        

       //DB::table('localisations')->delete();
        $locales = ['bepanda','akwa', 'Bali', 'Bonapriso'];

        foreach($locales as $locale) {
            Localisation::create(['name' => $locale]);
        }
    }
}
