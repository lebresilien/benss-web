<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'locale-list',
            'locale-create',
            'locale-edit',
            'locale-delete',
            'type-soin-list',
            'type-soin-create',
            'type-soin-edit',
            'type-soin-delete',
            'type-rdv-list',
            'type-rdv-create',
            'type-rdv-edit',
            'type-rdv-delete',
            'type-doctor-list',
            'type-doctor-create',
            'type-doctor-edit',
            'type-doctor-delete',
            'rdv-create',
            'rdv-list',
            'rdv-edit',
            'rdv-delete'
 
         ];

        foreach($permissions as $permission) {

            Permission::create(['name' => $permission]);

        }
    }
}
