<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeSoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_soins', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('type_doctor_id')->constrained('type_doctors')
                  ->onUpdate('cascade')->onDelete('cascade');
            $table->double('price', 8,2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_soins');
    }
}
