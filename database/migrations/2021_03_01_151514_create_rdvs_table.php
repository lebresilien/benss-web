<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRdvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdvs', function (Blueprint $table) {
            $table->id();
            $table->string('symptomes')->nullable();
            $table->unsignedBigInteger('nurse_id');
            $table->unsignedBigInteger('doctor_id');
            $table->text('observation')->nullable();
            $table->text('ordonnance')->nullable();
            $table->text('resultat')->nullable();
            $table->double('poids')->nullable();
            $table->double('taille')->nullable();
            $table->double('temperature')->nullable();
            $table->foreignId('user_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_soin_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->foreign('nurse_id')->references('id')->on('users');
            $table->foreignId('forfait_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('state')->default(false);
            $table->boolean('consult')->default(false);
            $table->timestamp('date_rdv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdvs');
    }
}
