<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('sexe')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('contact')->nullable();
            $table->string('profession')->nullable();
            $table->string('lieuNaissance')->nullable();
            $table->string('profile')->nullable();
            $table->string('role');
            $table->string('situation')->nullable();
            $table->date('dateNaisance')->nullable();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('localisation_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('forfait_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_doctor_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('active')->default(false);
            $table->boolean('busy')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
