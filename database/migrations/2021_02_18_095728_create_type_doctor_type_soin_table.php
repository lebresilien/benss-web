<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeDoctorTypeSoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_doctor_type_soin', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_doctor_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            //$table->foreignId('type_soin_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_tag', function(Blueprint $table) {
			$table->dropForeign('type_doctor_type_soin_type_doctor_id_foreign');
			$table->dropForeign('type_doctor_type_soin_type_soin_id_foreign');
		});
        Schema::dropIfExists('type_doctor_type_soin');
    }
}
