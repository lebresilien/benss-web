<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.min.css') }}">
      <link rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
  crossorigin="anonymous">
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
      <title>@yield('title', config('app.name'))</title>
      @include('admin.layouts.partials.include')
      @stack('base_link')
      @yield('styles')
  </head>
  <body>
    <div id="app">
      <div class="main-wrapper">
        @include('admin.layouts.partials.navbar')
        @include('admin.layouts.partials.sidebar')

        <!-- Main Content -->
        @yield('content')
        @include('admin.layouts.partials.footer')
      </div>
    </div>
    @stack('base_script')
    @yield('scripts')
  </body>
</html>
