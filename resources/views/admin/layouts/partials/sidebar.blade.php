<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{ url('/') }}">Benss</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Bs</a>
          </div>
          

          <ul class="sidebar-menu">
            @if(auth()->user()->role == 'admin')
              <li class="menu-header">Manager</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Utilisateurs</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('users.create') }}">Creation</a></li>
                  <li><a class="nav-link" href="{{ route('users.index') }}">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('users.listing', 'patient') }}">Listing Patients</a></li>
                  <li><a class="nav-link" href="{{ route('users.listing', 'medecin') }}">Listing Medecins</a></li>
                  <li><a href="{{ route('users.assign') }}" class="nav-link">Attribuer Patients</a></li>
                  <li><a class="nav-link" href="{{ route('users.listing', 'infirmier') }}">Listing Infirmiers</a></li>                  
                </ul>
              </li>
              
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-map-marked-alt"></i><span>Zones</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('locales.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('locales.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-user-md"></i><span>Types de médecins</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('type-doctors.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('type-doctors.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-stethoscope"></i><span>Types de Soins</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('type-soins.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('type-soins.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Types de Rdv</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('type-rdvs.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('type-rdvs.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Types d'interventions</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('type-interventions.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('type-interventions.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Services</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('services.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('services.create') }}">Creation</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Forfaits</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{ route('forfaits.index') }} ">Listing</a></li>
                  <li><a class="nav-link" href="{{ route('forfaits.create') }}">Creation</a></li>
                </ul>
              </li>
              
              

            @endif

            @if(auth()->user()->role == 'patient')
            
                @if(auth()->user()->user_id != NULL && auth()->user()->forfait_id != NULL)
                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>RDVs</span></a>
                      <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ route('rdvs.patient') }}">listing</a></li>
                        <li><a class="nav-link" href="{{ route('rdvs.create') }}">create</a></li>
                        <li><a class="nav-link" href="#" id="meet">contacter son medecin</a></li>
                      </ul>
                    </li>
                @endif

                <li class="nav-item dropdown">
                      <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Antecedents</span></a>
                      <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ route('antecedents.index') }}">listing</a></li>
                        <li><a class="nav-link" href="{{ route('antecedents.create') }}">create</a></li>
                      </ul>
                </li>

              <li class="menu-header">Compte</li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.profile') }}" class="nav-link"><i class="fas fa-book"></i> <span>Profile</span></a>
              </li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.password') }}" class="nav-link"><i class="fas fa-book"></i> <span>Changer votre password</span></a>
              </li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.medical') }}" class="nav-link"><i class="fas fa-book"></i> <span>Dossier medical</span></a>
              </li>
              @if(auth()->user()->forfait_id == NULL)
                <li class="nav-item dropdown">
                  <a href="{{ route('users.forfait') }}" class="nav-link"><i class="fas fa-book"></i> <span>Forfait</span></a>
                </li>
              @endif
            @endif

            @if(auth()->user()->role == 'medecin')

              <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Patients</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('users.doctor.patient') }}">listing</a></li>
                  </ul>
              </li>

              <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Interventions</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('rdvs.doctors') }}">listing</a></li>
                  </ul>
              </li>

              <li class="menu-header">Compte</li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.profile') }}" class="nav-link"><i class="fas fa-book"></i> <span>Profil</span></a>
              </li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.password') }}" class="nav-link"><i class="fas fa-book"></i> <span>Changer votre password</span></a>
              </li>
            @endif

            @if(auth()->user()->role == 'infirmier')

              <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>Patients</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('users.nurses.index') }}">listing</a></li>
                    <li><a class="nav-link" href="{{ route('users.nurses.create') }}">create</a></li>
                  </ul>
              </li>
              <li class="nav-item dropdown">
                  <a href="#" class="nav-link has-dropdown"><i class="far fa-calendar-check"></i><span>RDV</span></a>
                  <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('rdvs.nurses') }}">listing</a></li>
                  </ul>
              </li>

        

              <li class="menu-header">Compte</li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.profile') }}" class="nav-link"><i class="fas fa-book"></i> <span>Profile</span></a>
              </li>
              <li class="nav-item dropdown">
                <a href="{{ route('users.password') }}" class="nav-link"><i class="fas fa-book"></i> <span>Changer votre password</span></a>
              </li>
            @endif

           

            </ul>

           <!--  <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
              <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
              </a>
            </div> -->
          
        </aside>
      </div>