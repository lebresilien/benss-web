@push('base_link')
    <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/switchery.css') }}"></script>

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/components.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/custom.css') }}">

  <!-- Datatable css -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
  <!-- iziToast css -->
  <link rel="stylesheet" href="{{ asset('assets/admin/css/iziToast.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.min.css') }}">
  
@endpush
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
@push('base_script')
    <!-- General JS Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src=" {{ asset('assets/admin/js/stisla.js') }}"></script>

    <!-- JS Libraies -->
    <script src="{{ asset('js/Xhttp_ActiveX.js') }}"></script>
    <script src="{{ asset('assets/admin/js/switchery.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset('assets/admin/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/admin/js/custom.js') }}"></script>
    
    <!-- Page Datatable JS File -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('assets/admin/js/page/modules-datatables.js') }}"></script>
    <!-- iziToast js -->
    <script src="{{ asset('assets/admin/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/js/select2.min.js') }}"></script>
    <script>
      @if(session('store'))
        iziToast.success({
          title: 'Creation du {{ session('store') }}',
          message: '{{ session('store') }} Créé avec succès',
          position: 'topRight'
        });
      @endif
      @if(session('delete'))
        iziToast.warning({
          title: 'Suppression du {{ session('delete') }}',
          message: '{{ session('delete') }} Supprimé avec succès',
          position: 'topRight'
        });
      @endif
      @if(session('update'))
        iziToast.info({
          title: 'Modification  de {{ session('update') }}',
          message: '{{ session('update') }} Modifié avec succès',
          position: 'topRight'
        });
      @endif
    </script>
   
@endpush
