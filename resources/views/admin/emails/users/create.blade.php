<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Benss Cameroun - Création Compte</title>
    </head>
    <body style="font-size : 18px;">
        <p>Hello <b><span style="color:rgb(0,112,192); font-weight:blod;">{{ $user->name }},</span></b></p>
        <p>Votre compte  sur Benss Cameroun en tant que  <b>{{ $user->role }}</b> </p>
        <p>Avec pour adresse email: <b>{{ $user->email }} </p>
        <p>Et pour  mot de passe: <b>{{ $user->password }} </p>
        <p>a été crée avec succès.</p>
        <p>Bienvenue chez nous</p>
        <p>Merci pour votre confiance.</p>
        <p>A bientôt !</p>
        <p><b><span style="color:rgb(21, 61, 88); font-weight:bold;">L’équipe Benss Cameroun</span></b></p>
        <ul>
            <li><b>E-mail:</b>info@bensscameroun.com</li>
        </ul>
    </body>
</html>