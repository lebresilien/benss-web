<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Benss Cameroun - Confirmation Rendez-vous</title>
    </head>
    <body style="font-size : 18px;">
        <p>Hello</p>
        <p>Votre demande de rendez-vous pour: </p>
        <p><b>{{ $rdv->type_soin->name }} </a></p>
        @if($rdv->symptomes)
            <p>Avec les symptomes suivants : </p>
            <p><b>{{ $rdv->symptomes }}</b></p>
        @endif
        @if($rdv->forfait_id)
            <p>pour le service pré-enregistré </p>
            <p><b>{{ $rdv->service->name }}</b></p>
        @endif
        <p>a été confirmé pour le<b>{{ $rdv->date_rdv }} à {{ $rdv->date_rdv }} </p>
        <p>Cordialament</p>
        <p><b><span style="color:rgb(21, 61, 88); font-weight:bold;">L’équipe Benss Cameroun</span></b></p>
        <ul>
            <li><b>E-mail:</b> info@bensscameroun.com</li>
        </ul>
    </body>
</html>