@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Rdv_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Liste des interventions</h1>
            </div>

            <div class="section-body">
                <div class="section-body">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                    
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nom Patient</th>
                                <th>Nom Infirmier</th>
                                <th>Service</th>
                                <th>Date && Heure</th>
                                <th>Etat</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($rdvs as $rdv)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{  $rdv->user->name }} {{  $rdv->user->surname }} </td> 
                                <td>{{  $rdv->nurse->name }} {{  $rdv->nurse->surname }} </td>
                                <td>{{ $rdv->symptomes ? $rdv->symptomes : $rdv->service->name }}</td>
                                <td>{{ $rdv->date_rdv }} </td>
                                <td>
                                    @if($rdv->consult)
                                            <input type="checkbox" class="js-switch" checked /> 
                                    @else
                                            <input type="checkbox" class="js-switch" />  
                                    @endif
                                </td>
                                <td>
                                    @if($rdv->consult)
                                        <a class="btn btn-info btn-action mr-1" href="{{ route('soins.details',$rdv->id) }}" data-toggle="tooltip" title="" data-original-title="Show">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    @else
                                       <a class="btn btn-info btn-action mr-1" href="#" data-toggle="tooltip" title="" data-original-title="Show">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    @endif

                                    @if($rdv->consult)
                                        <a class="btn btn-primary btn-action mr-1"  href="#" data-toggle="tooltip" title="" data-original-title="">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @else
                                        <span class="btn btn-primary btn-action mr-1" offer="{{ $rdv->service->name }}"  name="doctor-treatment" key="{{ $rdv->id }}" nom="{{ $rdv->user->name }} {{ $rdv->user->surname }} "  data-toggle="tooltip" title="" id="{{ $rdv->user->id }}" data-original-title="">
                                            <i class="fas fa-pencil-alt"></i>
                                        </span>
                                    @endif

                                    {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                    data-toggle="tooltip" title="" 
                                    data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                    data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                    <i class="fas fa-trash"></i>
                                    </a>--}}
                                  
                                </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                </div>

        </section>

        <div class="modal" tabindex="-1" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Effectuez une consultation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="container-fluid">
                      
                        <div class="mb-3">
                            <label for="name" class="form-label">Nom du patient</label>
                            <input type="text" class="form-control" id="name" disabled />
                        </div>

                        <div class="mb-3">
                            <label for="name" class="form-label">Service du patient</label>
                            <input type="text" class="form-control" id="service" disabled />
                        </div>
                        
                        <div class="mb-3">
                            <label for="poids" class="form-label">Poids du patient</label>
                            <input type="text" class="form-control" id="poids" />
                        </div>

                        <div class="mb-3">
                            <label for="poids" class="form-label">Taille du patient</label>
                            <input type="text" class="form-control" id="taille" />
                        </div>

                        <div class="mb-3">
                            <label for="temp" class="form-label">Temperature du patient</label>
                            <input type="text" class="form-control" id="temp" />
                        </div>

                
                        <div class="mb-3">
                            <label for="obervation" class="form-label">Entrez les observations</label>
                            <textarea class="form-control" id="obervation"></textarea>
                        </div>

                        <div class="mb-3">
                            <label for="ordonnance" class="form-label">Entrez le elements de l'ordonnance</label>
                            <textarea class="form-control" id="ordonnance"></textarea>
                        </div>
                        @csrf
                        
                        
                     
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="close-modal">Close</button>
                    <button type="button" class="btn btn-primary" name="save">Enregistrez</button>
                </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(function(){

        let id = ""
        let nom = ""
        let key = ""
        let service = ""
        var elems = document.querySelectorAll('.js-switch');
        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i]);
            //if(elems[i].getAttribute('name') == "checked")
               switchery.disable()
            
        }


        $('[name="doctor-treatment"]').click(function(){

           id = $(this).attr('id')
           nom = $(this).attr('nom')
           key = $(this).attr('key')
           service = $(this).attr('offer')
           $('#name').val(nom)
           $('#service').val(service)
          $('#modal').modal('show')
          
        })

        $('#close-modal').click(function(){
            $('#modal').modal('hide')
        })

      $('#check').click(function(){
          if($(this).prop('checked'))
          {
             $('#container-intervention').css('display','block')
          }else{
            $('#container-intervention').css('display','none')
          }
      })

      $('[name="save"]').on('click', function(){

         const observation = $('#obervation').val()
         const ordonnance = $('#ordonnance').val()
         const taille = $('#taille').val()
         const poids = $('#poids').val()
         const temp = $('#temp').val()
         const token = $('[name="_token"]').val()

         save(id, observation, ordonnance, taille, poids, temp, key, token)
        
      })
    })


    function save(id, observation,ordonnance, taille="", poids="", temp="", key, token){

        var xhr = getXMLHttp();
        var url = "{{ route('soins.store') }}"
                
        xhr.onreadystatechange = function()    
            {
                $('[name="save"]').text('Entregistrement....')
                if(xhr.readyState == 4)
                    {    
                        if(xhr.status == 200)
                        {
                                var info = xhr.responseText;
                                var json = eval('('+info+')');
                                $('[name="save"]').text('Entregistrez')
                                $('#modal').modal('hide')
                                location.reload()
                           
                        }
                    }     
            }

        xhr.open("POST", url , false);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("user_id="+id+"&observation="+observation+"&temp="+temp+"&poids="+poids+"&taille="+taille+"&ordonnance="+ordonnance+"&key="+key+"&_token="+token);
    }
   

</script>
@endsection 