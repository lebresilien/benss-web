@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Users_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Utilisateurs</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.rdvs.form')
            </div>
        </section>
    </div>
@endsection