<?php
if ($rdv->id) {
    $options = ['method' => 'put', 'url' => route('rdvs.update',$rdv)];
} else {
    $options = ['method' => 'post', 'url' => route('rdvs.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($rdv, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf
<div class="card">
    <div class="card-header">
        <h4>Ajouter un Rdv</h4>
    </div>
</div>

@if (\Session::has('status'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('status') !!}</li>
        </ul>
    </div>
@endif


@if(auth()->user()->forfait_id == NULL && auth()->user()->role == 'patient'  )
    <div class="alert alert-warning" role="alert">
         Vous n'avez pas de forfait vous serrez facturer lors du rendez-vous
    </div>
@endif

<div class="row">
    <div class="col-md-10 col-12 offset-md-1">
        <div class="card">
            <div class="card-body">

                <div class="form-group">

                    {!! Form::label('type', 'Type Rdv', ['class' => 'control-label']) !!}
                    {!! Form::select('type', $services->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                </div> 
      
                @if(auth()->user()->role == 'patient')
                
                    <div class="form-group">

                        {!! Form::label('nurse', 'Selectionnez l\infirmier', ['class' => 'control-label']) !!}
                        {!! Form::select('nurse', $nurses->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                    </div>

                @endif

                <div class="form-group">
                    {!! Form::label('date_rdv', 'Date Rdv', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::date('date_rdv', \Carbon\Carbon::now(), ['class' => 'form-control '.( $errors->has('date_rdv') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your Date Rdv
                            @error('date_rdv')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>                   
                </div>

                <div class="form-group">
                    {!! Form::label('heure_rdv', 'Heure Rdv', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::time('heure_rdv', \Carbon\Carbon::now(), ['class' => 'form-control '.( $errors->has('heure_rdv') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your Heure Rdv
                            @error('heure_rdv')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>      
                </div>
              
                @if(auth()->user()->forfait_id == NULL  || auth()->user()->role == 'infirmier')

                    <div class="form-group" style="display:none" id="id-symptomes">
                        {!! Form::label('symptomes', 'Listez les raisons de votre rendez-vous', ['class' => 'control-label']) !!}
                        {!! Form::textarea('symptomes', null, ['class' => 'form-control '.( $errors->has('symptomes') ? 'is-invalid' :''),'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your symptomes
                            @error('symptomes')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                @endif

                   
                </div>
            </div>
        </div>
    </div>
    
</div>


<div class="card">
    <div class="card-body">
      
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit" name="create">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
