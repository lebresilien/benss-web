@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Rdv_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Rdv</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.rdvs.form')
            </div>
        </section>
    </div>
@endsection

@section('scripts')

   <script type="text/javascript">
       $(function(){
      
           if($('[name="type_in"]').val() == "autres")
           {
               $('#id-symptomes').css('display','block')
           }
           
            $('[name="type_in"]').change(function(){

                if($(this).val() == "autres")
                {
                    $('#id-symptomes').css('display','block')
                }
                else{
                    $('#id-symptomes').css('display','none')
                }
            })

       })
   </script>
@endsection

