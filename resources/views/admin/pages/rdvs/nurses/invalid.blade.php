@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Rdv_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Liste des Rendez-vous Non Validés</h1>
            </div>

            <div class="section-body">
                <div class="section-body">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                   
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th class="text-center">
                                #
                                </th>
                                <th>Nom Patient</th>
                                <th>Nom Medecin</th>
                                <th>Service</th>
                                <th>Date && Heure</th>
                                <th>Etat</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($rdvs as $rdv)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{  $rdv->user->name }} {{  $rdv->user->surname }} </td> 
                                <td>{{  $rdv->doctor->name }} {{  $rdv->doctor->surname }} </td> 
                                <td>{{ $rdv->symptomes ? $rdv->symptomes : $rdv->service->name }}</td>
                                <td>{{ $rdv->date_rdv }} </td>
                                <td>@if($rdv->state)
                                            <input type="checkbox" class="js-switch" checked name="checked" value="1" /> 
                                    @else
                                            <input type="checkbox" class="js-switch"  name="checkbox" value="{{ $rdv->id }}"  />  
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-info btn-action mr-1" href="#" data-toggle="tooltip" title="" data-original-title="Show">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    @if($rdv->state)
                                        <a class="btn btn-primary btn-action mr-1"  href="#" data-toggle="tooltip" title="" data-original-title="Edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-primary btn-action mr-1"  href="{{ route('rdvs.edit', $rdv) }}" data-toggle="tooltip" title="" data-original-title="Edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @endif

                                    {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                    data-toggle="tooltip" title="" 
                                    data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                    data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                    <i class="fas fa-trash"></i>
                                    </a>--}}
                                    @if(!$rdv->state)

                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => route('rdvs.destroy', $rdv),
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fas fa-trash"></i> ', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger',
                                                'title' => 'Rendez-vous',
                                            )) !!}
                                        {!! Form::close() !!}

                                    @endif
                                </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                </div>
        </section>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(function(){

        var elems = document.querySelectorAll('.js-switch');
        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i]);
            if(elems[i].getAttribute('name') == "checked")
               switchery.disable()
            
        }

        

        $('[name="checkbox"]').each(function(){

          $(this).change(function(){
            confirm($(this).val())
          }) 
            
        })
    })

    function confirm(id){

        var xhr = getXMLHttp();
        var url = "/accueil/rdvs/confirm/"+id
                
        xhr.onreadystatechange = function()    
            {
                if(xhr.readyState == 4)
                    {    
                        if(xhr.status == 200)
                        {
                                var info = xhr.responseText;
                                location.reload()
                              

                        }
                    }     
            }
        
        xhr.open("GET", url , false);
        xhr.send(null);
    }
   

</script>
@endsection 