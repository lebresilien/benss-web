@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Interventions_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Ajouter une intervention</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.type_interventions.form')
            </div>
        </section>
    </div>
@endsection