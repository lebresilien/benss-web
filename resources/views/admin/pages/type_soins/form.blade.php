<?php
if ($type->id) {
    $options = ['method' => 'put', 'url' => route('type-soins.update',$type)];
} else {
    $options = ['method' => 'post', 'url' => route('type-soins.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($type, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom du type de soin') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nom du type de soin"]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('type_doctor_id', 'type medecin') !!}
            {!! Form::select('type_doctor_id', $types->pluck('name', 'id'), null, ['class' => 'select2', "data-height"=>"100%", "data-width"=>"100%"]); !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Prix') !!}
            {!! Form::text('price', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Prix"]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
