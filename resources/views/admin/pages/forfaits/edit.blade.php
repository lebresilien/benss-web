@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Categories_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Modification Forfait</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.forfait.form')
            </div>
        </section>
    </div>
@endsection