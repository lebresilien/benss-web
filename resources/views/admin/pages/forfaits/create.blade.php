@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Forfait_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Ajouter un forfait</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.forfaits.form')
            </div>
        </section>
    </div>
@endsection