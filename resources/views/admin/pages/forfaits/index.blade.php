@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Forfait_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Liste des forfaits</h1>
            </div>

            <div class="section-body">
                <div class="section-body">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        <a href="{{ route('forfaits.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nouveau</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Prix</th>
                                <th>Nombre visite</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($forfaits as $forfait)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $forfait->name }}</td>
                                <td>{{ $forfait->price }} FCFA</td>
                                <td>{{ $forfait->nbre_visite }}</td>
                                <td>
                                    <a class="btn btn-info btn-action mr-1" href="{{ route('forfaits.show', $forfait) }}" data-toggle="tooltip" title="" data-original-title="Show">
                                    <i class="fas fa-eye"></i>
                                    </a>
                                    <a class="btn btn-primary btn-action mr-1" href="{{ route('forfaits.edit', $forfait) }}" data-toggle="tooltip" title="" data-original-title="Edit">
                                    <i class="fas fa-pencil-alt"></i>
                                    </a>
                                    {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                    data-toggle="tooltip" title="" 
                                    data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                    data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                    <i class="fas fa-trash"></i>
                                    </a>--}}
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => route('forfaits.destroy', $forfait),
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger',
                                        'title' => 'Role',
                                    )) !!}
                            {!! Form::close() !!}
                                </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                </div>
        </section>
    </div>
@endsection@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Forfait_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Liste des forfaits</h1>
            </div>

            <div class="section-body">
                <div class="section-body">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        <a href="{{ route('forfaits.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nouveau</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th class="text-center">
                                #
                                </th>
                                <th>Name</th>
                                <th>Prix</th>
                                <th>Nombre visite</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($forfaits as $forfait)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $forfait->name }}</td>
                                <td>{{ $forfait->price }} FCFA</td>
                                <td>{{ $forfait->nbre_visite }}</td>
                                <td>
                                    <a class="btn btn-info btn-action mr-1" href="{{ route('forfaits.show', $forfait) }}" data-toggle="tooltip" title="" data-original-title="Show">
                                    <i class="fas fa-eye"></i>
                                    </a>
                                   <!--  <a class="btn btn-primary btn-action mr-1" href="{{ route('forfaits.edit', $forfait) }}" data-toggle="tooltip" title="" data-original-title="Edit">
                                    <i class="fas fa-pencil-alt"></i>
                                    </a> -->
                                    {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                    data-toggle="tooltip" title="" 
                                    data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                    data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                    <i class="fas fa-trash"></i>
                                    </a>--}}
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => route('forfaits.destroy', $forfait),
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i> ', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger',
                                        'title' => 'Role',
                                    )) !!}
                            {!! Form::close() !!}
                                </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                </div>
        </section>
    </div>
@endsection