<?php
if ($forfait->id) {
    $options = ['method' => 'put', 'url' => route('forfaits.update',$forfait)];
} else {
    $options = ['method' => 'post', 'url' => route('forfaits.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($forfait, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom du forfait') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nom du forfait"]) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('service', 'Ajouter des services à ce forfait') !!}
            {!! Form::select('services[]', $services->pluck('name', 'id'), null , ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Prix du forfait') !!}
            {!! Form::text('price', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Prix du forfait"]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nbre_viste', 'Nombre visites par mois du forfait') !!}
            {!! Form::text('nbre_visite', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nombre de visites par mois du forfait"]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description du forfait') !!}
            {!! Form::textarea('description', null , ['class' => 'form-control', 'placeholder'=>"description du forfait"]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
