@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Medical')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Detail Forfait</h1>
            </div>

            <div class="section-body">

              

               <div class="row">
                   <div class="col-md-10 offset-md-1 col-12">
                      <div class="card">
                           <div class="card-title offset-4">
                              <h2>{{ $forfait->name }}</h2>
                           </div>
                           <div class="card-body">
                               
                           @forelse($forfait->services as $service)

                                <p class="section-lead">
                                  <span class="section-title">{{ $service->name}}</span>:
          
                                </p>
                            @empty
                            @endforelse
                           </div>
                      </div>
                   </div>
                  </div>
                  
            </div>
        </section>
    </div>
@endsection