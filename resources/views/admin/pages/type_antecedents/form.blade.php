<?php
if ($type->id) {
    $options = ['method' => 'put', 'url' => route('type-antecedents.update',$type)];
} else {
    $options = ['method' => 'post', 'url' => route('type-antecedents.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($type, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom du type de RDV') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nom du type d'antecedent"]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
