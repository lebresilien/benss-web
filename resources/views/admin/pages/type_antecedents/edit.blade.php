@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Rdv_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Modifier le type d'antecedent medical</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.type_antecedents.form')
            </div>
        </section>
    </div>
@endsection