@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Doctor_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Modifier un type</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.type_doctors.form')
            </div>
        </section>
    </div>
@endsection