@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Antecedent')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Ajouter un antecedent medical</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.antecedents.form')
            </div>
        </section>
    </div>
@endsection