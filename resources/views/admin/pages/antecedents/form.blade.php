<?php
if ($antecedent->id) {
    $options = ['method' => 'put', 'url' => route('antecedents.update',$antecedent)];
} else {
    $options = ['method' => 'post', 'url' => route('antecedents.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($antecedent, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom du type de RDV') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nom du type d'antecedent"]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('type_antecedent_id', 'Type antecedent', ['class' => 'control-label']) !!}
            {!! Form::select('type_antecedent_id', $type_antecedent->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!} 
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
