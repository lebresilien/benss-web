@extends('admin.layouts.app')
@section('title', 'Administration Password')
@section('content')

<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Profile</h1>
            </div>

            @if(session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error')}}
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success')}}
                </div>
            @endif
           

            <div class="section-body">
                <div class="row">
                    <div class="col-sm-8 offset-sm-2">
                        <div class="card">
                                <div class="card-body">
                                {!! Form::open(['url' => route('users.password'), 'class'=>'needs-validation', 'novalidate'=>'']) !!}
                                    <div class="form-group">
                                        @csrf
                                    
                                        {!! Form::label('motdepasse', 'Entrer votre mot de passe') !!}
                                        {!! Form::password('motdepasse',['class' => 'form-control '.( $errors->has('motdepasse') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                                        <div class="invalid-feedback">
                                            Please fill in your motdepasse
                                        </div>
                                        @error('motdepasse')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                <div class="form-group">
                                    <div class="d-block">
                                        {!! Form::label('password', 'Nouveau mot de passe', ['class' => 'control-label']) !!}
                                    </div>
                                    {!! Form::password('password', ['class' => 'form-control '.( $errors->has('password') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2"] ) !!}
                                    <div class="invalid-feedback">
                                    please fill in your password
                                    </div>
                                    @error('password')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                </div>
                                <div class="form-group">
                                    <div class="d-block">
                                        {!! Form::label('password_confirmation', 'Confimez votre mot de passe', ['class' => 'control-label']) !!}
                                    </div>
                                    {!! Form::password('password_confirmation', ['class' => 'form-control '.( $errors->has('password_confirmation') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"3"] ) !!}
                                    <div class="invalid-feedback">
                                    please fill in your password confirmation
                                    </div>
                                    @error('password_confirmation')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    {!! Form::submit('Sauvegarder', ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex'=>'4']) !!}
                                </div>
                            {!! Form::close() !!}
                                </div>
                        </div>
                    </div>
                </div>
                
            </div>
    </section>
</div>
@endsection

@section('scripts')
  
@endsection