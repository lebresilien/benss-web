@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Users_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Utilisateurs</h1>
            </div>

            <div class="section-body">

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 order-lg-1">
                            <div class="card shadow mb-4">
                                
                                <div class="card-body">
                                    {!! Form::model(['route' => 'assign.patients'], ['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
                                        @csrf
                                        <div class="pl-lg-4">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        {!! Form::label('doctor', 'Selectionner le medecin', ['class' => 'control-label']) !!}
                                                {!! Form::select('doctor', $doctors->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        {!! Form::label('patient', 'Selectionner le(s) patient(s)', ['class' => 'control-label']) !!}
                                                {!! Form::select('patient[]', [],null,['class' => 'form-control select2'.( $errors->has('patient') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"2", "multiple"=>"multiple", 'id'=>'patient'] ) !!}
                                                        <div class="invalid-feedback">
                                                            Veuillez selectionner un patient
                                                            @error('patient')
                                                                {{ $message }}
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pl-lg-4">
                                            <div class="row">
                                                <div class="col text-center">
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                    <button class="btn btn-primary" type="submit" name="create">Validé</button>
                                                </div>
                                            </div>
                                        </div>
                                    {!! Form::close() !!} 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">

   $(function(){
     
       const id = $('[name="doctor"]').val()
       assign(id)

       $('[name="doctor"]').change(function(){
           assign($(this).val())
       })
   })


   function assign(id)
    {
      
        var xhr = getXMLHttp();
        var url = "../../users/assign/"+id
        
        xhr.onreadystatechange = function()    
            {
                if(xhr.readyState == 4)
                    {    
                        if(xhr.status == 200)
                        {
                                var info = xhr.responseText;
                                var json = eval('('+info+')');
                               
                                placerPatients(json.patients) 

                        }
                    }     
            }
        
        xhr.open("GET", url , false);
        xhr.send(null);
    }


   function placerPatients(json)
    {
        var id;
        var name;
        var surname;
        $('#patient').children().remove()
    
        if(json.length > 0 )
        { 
            for(var i = 0 ; i < json.length ; i++)
            {

                id = json[i].id;
                name = json[i].name;
                surname = json[i].surname;
                $('<option value="'+id+'">'+name+ ' ' + surname + '</option>').appendTo('#patient');
            }
           
        }
    }
</script>
@endsection