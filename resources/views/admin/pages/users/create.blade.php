@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Users_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Utilisateurs</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.users.form')
            </div>
        </section>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">

   $(function(){

       if($('[name="role"]').val() == "medecin")
        {
            $('#zone-container, #nurse-container, #type-container').css('display','block')
            $('#doctor-container').css('display', 'none')
            loadData($('[name="zone"]').val(), $('[name="role"]').val())
        }

        if($('[name="role"]').val() == "infirmier")
        {
            $('#nurse-container, #type-container, #patients-container').css('display','none')
            $('#zone-container,#doctor-container').css('display', 'block')
            
           // loadData($('[name="zone"]').val(), $('[name="role"]').val())
        }

       $('[name="role"]').change(function(){

          if($(this).val() == "medecin")
          {
            $('#zone-container, #nurse-container, #nurses-container, #type-container').css('display','block')
            $('#doctor-container').css('display', 'none')
            loadData($('[name="zone"]').val(), $('[name="role"]').val())
           
          }
          else if($(this).val() == "infirmier")
          {
            $('#nurses-container, #type-container, #patients-container').css('display','none')
            $('#zone-container, #doctor-container').css('display','block')
            loadData($('[name="zone"]').val(), $('[name="role"]').val())
           
          }
         else{
            $('#zone-container,#patients-container,#nurses-container,#nurse-container, #type-container, #doctor-container').css('display','none')
          }

       })

       $('[name="zone"]').change(function(){
        
         const id = $(this).val();
         const role = $('[name="role"]').val()
         loadData(id, role)

       })

       $('#nurse-container').click(function(){

        const name = $(this).attr('name')

        /* if(name == "0")
        {
            $('[data-target="#demo"]').text('Annuler')
            $(this).attr('name','1')
        } 
        else $('[data-target="#demo"]').text('Ajouter un infirmier') */

       })

    })

    function loadData(id, role)
    {
        if(role === "medecin")
        {
            $('#patients-container, #nurses-container').css('display','block')
        }
       

        var xhr = getXMLHttp();
        var url = "../users/zone/"+id+"/"+role
        
        xhr.onreadystatechange = function()    
            {
                if(xhr.readyState == 4)
                    {    
                        if(xhr.status == 200)
                        {
                                var info = xhr.responseText;
                                var json = eval('('+info+')');
                                if(role === "medecin")
                                {
                                    placerPatients(json.patients[0])
                                    placerNurses(json.nurses[0])
                                }else{
                                    placerDoctors(json.doctors[0])
                                }
                                
                              

                        }
                    }     
            }
        
        xhr.open("GET", url , false);
        xhr.send(null);
    }


    function placerPatients(json)
    {
        
        var id;
        var name;
        var surname;
        $('#patients').children().remove()
        if(json.length > 0 )
        {
            
            //$('[name="patients"]').css('display','block')
            //$('<label>test</label><select name="select-patients" multiple class="form-control select2"></select>').insertAfter('[name="role"]')

            for(var i = 0 ; i < json.length ; i++)
            {

                id = json[i].id;
                name = json[i].name;
                surname = json[i].surname;
                $('<option value="'+id+'">'+name+ ' ' + surname + '</option>').appendTo('#patients');
            }
        }
    }


    function placerNurses(json)
    {
        var id;
        var name;
        var surname;
        $('[name="nurses"]').children().next().remove()
    
        if(json.length > 0 )
        {
            //$('[name="nurses"]').children().remove()
            //$('<option value="0">Choisir un infirmier...</option>').appendTo('[name="nurses"]')
            //$('[name="nurses"]').css('display','block')
            //$('<label>ggg</label><select name="select-nurses" class="form-control select2"></select>').insertAfter('#')

            for(var i = 0 ; i < json.length ; i++)
            {

                id = json[i].id;
                name = json[i].name;
                surname = json[i].surname;
                $('<option value="'+id+'">'+name+ ' ' + surname + '</option>').appendTo('[name="nurses"]');
            }
        }
    }


    function placerDoctors(json)
    {
        var id;
        var name;
        var surname;
        console.log(json)
    
        if(json.length > 0 )
        {
            
            for(var i = 0 ; i < json.length ; i++)
            {

                id = json[i].id;
                name = json[i].name;
                surname = json[i].surname;
                $('<option value="'+id+'">'+name+ ' ' + surname + '</option>').appendTo('[name="doctor"]');
            }
        }
    }
</script>
@endsection