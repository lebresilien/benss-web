@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Update')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Profil</h1>
            </div>

            @if(session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error')}}
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success')}}
                </div>
            @endif

            <div class="section-body">
                
                    
                {!! Form::model($user, ['method' => 'put', 'url' => route('users.profile.update'), 'files' => true]) !!}
                   @csrf
                   <div class="row">

                       <div class="col-sm-10 offset-sm-1">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-7 col-12">
                                            <div class="form-group">
                                                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('name', null, ['class' => 'form-control '.( $errors->has('name') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le nom
                                                        @error('name')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('surname', 'Surname', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('surname', null, ['class' => 'form-control '.( $errors->has('surname') ? 'is-invalid' :''), 'tabindex'=>"2"] ) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le prenom
                                                        @error('surname')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('contact', 'Telephone', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('contact', null, ['class' => 'form-control '.( $errors->has('contact') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2"] ) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le contact
                                                        @error('contact')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            @if(auth()->user()->role == 'patient')
                                                <div class="form-group">
                                                    {!! Form::label('profession', 'Profession', ['class' => 'control-label']) !!}
                                                    <div class="">
                                                        {!! Form::text('profession', null, ['class' => 'form-control '.( $errors->has('profession') ? 'is-invalid' :''), 'tabindex'=>"2"] ) !!}
                                                        <div class="invalid-feedback">
                                                            Remplissez la profession
                                                            @error('profession')
                                                                {{ $message }}
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                {!! Form::label('dateNaisance', 'Date Naissance', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('dateNaisance', null, ['class' => 'form-control '.( $errors->has('dateNaisance') ? 'is-invalid' :''), 'id'=>"date_naissance"] ) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le date naissance
                                                        @error('dateNaissance')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('lieuNaissance', 'Lieu Naissance', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('lieuNaissance', null, ['class' => 'form-control '.( $errors->has('lieuNaissance') ? 'is-invalid' :''), 'tabindex'=>"2"] ) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le lieu naissance
                                                        @error('lieuNaissance')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('sexe', 'Sexe', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('sexe', null, ['class' => 'form-control', 'id'=>"sexe"] ) !!}
  
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('situation', 'Situation familiale', ['class' => 'control-label']) !!}
                                                <div class="">
                                                    {!! Form::text('situation', null, ['class' => 'form-control', 'id'=>"sexe"] ) !!}
  
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-5 col-12">

                                         <div class="row">
                                                <div class="col-sm-8 offset-sm-2 mt-3">    
                                                    @if(auth()->user()->profile)
                                                    <img class="rounded-circle" width="200" height="200" src="{{ asset('public/assets/admin/uploads/'.auth()->user()->profile) }}" />
                                                    @else
                                                    <img class="rounded-circle" width="200" height="200"  src="{{ asset('public/assets/admin/img/avatar/avatar-1.png') }}" />
                                                    @endif
                                                </div>
                                            </div>
    
                                            <div class="row">
                                                <div class="col-sm-12 mt-2 ">
                                                    {!! Form::file('file', ['class' => 'form-control'.( $errors->has('file') ? 'is-invalid' :''), 'name'=>"file"]) !!}
                                                    <div class="invalid-feedback">
                                                        Remplissez le fichier
                                                        @error('file')
                                                            {{ $message }}
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                   


                                </div>
                            </div>
                       </div>

                      
                   </div>

                   <div class="card">
                        <div class="card-body">
                        
                            <div class="card">
                                <div class="card-footer text-right">
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                    <button class="btn btn-primary" type="submit" name="create">Validé</button>
                                </div>
                            </div>
                        </div>
                   </div>
                {!! Form::close() !!}
                
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function(){

            $('#sexe').click(function(){
                $(this).replaceWith('<select name="sexe" class="form-control select2"><option value="H">Homme</option><option value="F">Femme</option></select>')
            })

            $('#date_naissance').click(function(){
                $(this).replaceWith('<input type="date" name="dateNaisance" class="form-control" />')
            })

            const validImageTypes = ["image/jpeg", "image/png"];
            $('[name="file"]').change(function(){

                const file = this.files[0];
                const fileType = file["type"];


                if($.inArray(fileType, validImageTypes) < 0) {
                    alert('image invalide')
                }
                else{
                     $(this).attr('src',this.val())
                }
                
                
            })

            
        })
    </script>
@endsection('scripts')