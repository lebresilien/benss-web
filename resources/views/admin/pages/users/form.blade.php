<?php
if ($user->id) {
    $options = ['method' => 'put', 'url' => route('users.update',$user)];
} else {
    $options = ['method' => 'post', 'url' => route('users.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($user, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf


@if (\Session::has('error'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::text('name', null, ['class' => 'form-control '.( $errors->has('name') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your name
                            @error('name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>

                    {!! Form::label('surname', 'Prenom', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::text('surname', null, ['class' => 'form-control '.( $errors->has('surname') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your surname
                            @error('surname')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>

                    {!! Form::label('email', 'Email', ['class' => 'col-sm-7 control-label']) !!}
                    <div class="">
                        {!! Form::text('email', null, ['class' => 'form-control '.( $errors->has('email') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your email
                            @error('email')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>

                    {!! Form::label('contact', 'Telephone', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::text('contact', null, ['class' => 'form-control '.( $errors->has('contact') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your email
                            @error('contact')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div id="zone-container" style="display:none">
                        {!! Form::label('locale', 'Zone', ['class' => 'control-label']) !!}
                        {!! Form::select('locale', $locales->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true", 'name' => 'zone'] ) !!} 
                    </div>

                    <hr/>

                    {!! Form::label('password', 'Mot de passe', ['class' => 'control-label']) !!}
                    <div class="">
                    {!! Form::text('password', null, ['class' => 'form-control '.( $errors->has('password') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your password
                            @error('password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>

                    {!! Form::label('password_confirmation', 'Confimez votre mot de passe', ['class' => 'control-label']) !!}
                    <div class="">
                        {!! Form::text('password_confirmation', null, ['class' => 'form-control '.( $errors->has('password_confirmation') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your password confirmmation
                            @error('password_confirmation')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    
                    {!! Form::label('role', 'Role', ['class' => 'control-label']) !!}
                    {!! Form::select('role', $roles->pluck('name', 'name'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true", "name" => "role"] ) !!}

                    <div class="" style="display:none" id="type-container">
                        {!! Form::label('type', 'Type medecin', ['class' => 'control-label']) !!}
                        {!! Form::select('type', $types->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                    </div>

                    <div id="nurses-container" style="display:none">
                    {!! Form::label('nurses', 'Attribuez des infirmiers', ['class' => 'control-label']) !!}
                    {!! Form::select('nurses', ['0' => 'Choisir un infirmier...'],null,['class' => 'form-control select2','required' => 'required', 'tabindex'=>"2", "autofocus"=>"true", "name" => "nurses"] ) !!}
                    </div>

                    <div id="patients-container" style="display:none">
                        {!! Form::label('patients', 'Attribuez des patients', ['class' => 'control-label']) !!}
                        {!! Form::select('patients[]', [],null,['class' => 'form-control select2', 'tabindex'=>"2", "autofocus"=>"true", "id" => "patients" , "multiple" => "multiple"] ) !!}
                    </div> 
                    

                    <div id="doctor-container" style="display:none">
                    {!! Form::label('doctor', 'Attribuez un medecin', ['class' => 'control-label']) !!}
                    {!! Form::select('doctor', [],null,['class' => 'form-control select2', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                    </div>
                
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div style="display:none" id="nurse-container" name="0">

    <div class="card">
        <div class="card-body">
            <div class="row">
            <span class="btn btn-primary" data-toggle="collapse" data-target="#demo"  type="button">
                  Ajouter un infirmier
            </span>
            </div>
        </div>
    </div>

    <div class="row" id="demo" class="collapse">
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('namef', 'Nom Infirmier', ['class' => 'control-label']) !!}
                        <div class="">
                            {!! Form::text('namef', null, ['class' => 'form-control '.( $errors->has('namef') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your name
                                @error('namef')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        {!! Form::label('contactf', 'Telephone Infirmier', ['class' => 'control-label']) !!}
                        <div class="">
                            {!! Form::text('contactf', null, ['class' => 'form-control '.( $errors->has('contactf') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your contact
                                @error('contactf')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>

                        {!! Form::label('passwordf', 'Mot de passe', ['class' => 'control-label']) !!}
                        <div class="">
                        {!! Form::text('passwordf', null, ['class' => 'form-control '.( $errors->has('passwordf') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your password
                                @error('passwordf')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        {!! Form::hidden('rolef', 'infirmier' ) !!}
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('surnamef', 'Prenom Infirmier', ['class' => 'control-label']) !!}
                        <div class="">
                            {!! Form::text('surnamef', null, ['class' => 'form-control '.( $errors->has('surnamef') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your surname
                                @error('surname')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        {!! Form::label('emailf', 'Email Infirmier', ['class' => 'col-sm-7 control-label']) !!}
                        <div class="">
                            {!! Form::text('emailf', null, ['class' => 'form-control '.( $errors->has('emailf') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your email
                                @error('emailf')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                        {!! Form::label('password_confirmationf', 'Confimez votre mot de passe', ['class' => 'col-sm-7 control-label']) !!}
                        <div class="">
                            {!! Form::text('password_confirmationf', null, ['class' => 'form-control '.( $errors->has('password_confirmationf') ? 'is-invalid' :''), 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                            <div class="invalid-feedback">
                                Please fill in your password confirmmation
                                @error('password_confirmation')
                                    {{ $message }}
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="card">
    <div class="card-body">
      
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit" name="create">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
