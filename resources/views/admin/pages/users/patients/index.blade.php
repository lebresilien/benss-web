@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Patients_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Liste des patients</h1>
                <input type="hidden" id="jitsi-name" value="{{ auth()->user()->name }}" />
          	    <input type="hidden" id="jitsi-email" value="{{ auth()->user()->email }}" />
            </div>

            <div class="section-body">
                <div class="section-body">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                   
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Noms et Prenoms</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Zone</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }} {{ $user->surname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->localisation ? $user->localisation->name : '' }}</td>
                                <td>
                                    <a class="btn btn-info btn-action mr-1" href="{{ route('users.show', $user)  }}" data-toggle="tooltip" title="" data-original-title="Details">
                                    <i class="fas fa-eye"></i>
                                    </a>
                                    <a class="btn btn-info btn-action mr-1" href="{{ route('users.medical.users', $user)  }}" data-toggle="tooltip" title="" data-original-title="Dossier medical">
                                    <i class="fas fa-book-open"></i>
                                    </a>
                                    <a class="btn btn-info btn-action mr-1 meet" href="#" data-toggle="tooltip" title="" data-original-title="Contacter">
                                    <i class="fas fa-phone-volume"></i>
                                    </a>
                                    {{--<a class="btn btn-info btn-action mr-1"  href="#" data-toggle="tooltip" title="" data-original-title="Fermer Jitsi" style="display:none">
                                    <i class="fas fa-times-circle"></i>
                                    </a>--}}
                                   
                                    {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                    data-toggle="tooltip" title="" 
                                    data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                    data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                    <i class="fas fa-trash"></i>
                                    </a>--}}
                                   
                                </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                </div>
            <div class="row" style="display:none" id="container-jitsi">
                <div class="col-sm-10 offset-sm-1">
                    <button type="button" class="btn btn-primary btn-lg btn-block" id="close-jitsi">Fermer Jitsi</button>
                </div>
          	</div>
            <div class="row mt-3">
                 <div class="col-sm-8 offset-sm-2">
                     <div id="jitsi-meet"></div>
                 </div>
          	</div>
        </section>
    </div>
@endsection

@section('scripts')

    <script src='https://meet.jit.si/external_api.js'></script>
    <script type="text/javascript">
        $(function(){
            
            $('.meet').each(function(){
                
                $(this).on('click', function(){
               
                    $('#container-jitsi').css('display','')
     
                    const domain = 'meet.jit.si';
                    const options = {
                        configOverwrite: { startWithAudioMuted: false },
                        userInfo: {
                            email: $('#jitsi-email').val(),
                            displayName: $('#jitsi-name').val()
                        },
                        roomName: 'Prise Rendez-vous',
                        width: 700,
                        height: 700,
                        parentNode: document.querySelector('#jitsi-meet')
                    };
                    const api = new JitsiMeetExternalAPI(domain, options);
                    //api.executeCommand('toggleAudio');
                    
                    $('#close-jitsi').click(function(){
                        $('#jitsi-meet').empty()
                        $(this).parent().parent().css('display', 'none')
                    })
                })
           
           })
            
           
        })
    </script>
   
@endsection