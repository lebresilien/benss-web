@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Subscrition')

@section('styles')

   <style>
       #selected{
           
       }
   </style>
@endsection


@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Souscription</h1>
            </div>

            <div class="row" style="display:none">
               <div class="card">
                     <div class="card-body" id="detais-forfait">
                        
                     </div>
               </div>
            </div>

            <div class="row" style="display:none">
               <div class="card">
                     <div class="card-body" id="elements-forfait">
                        
                     </div>
               </div>
            </div>

            <div class="section-body">
              <input type="hidden" name="forfait_price" />
              <div class='row'>
                <div class='col-md-2'></div>
                <div class='col-md-8'>
                  <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
                  <form accept-charset="UTF-8" action="{{route('users.forfait.store')}}" class="require-validation" data-cc-on-file="false" 
                    data-stripe-publishable-key="pk_test_51IWNxtKEfvkV8aGdxApdNwOLDi6IM0dvii541Vj53rO573Bnf9VDKfAi7tDmO8803a5YcgCXq2uNEIP9osnq3zWl00mVyfCrdZ" id="payment-form" method="post">
                    {{ csrf_field() }}
                    <div class="pl-lg-4">
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label class="form-control-label" for="forfait">Selectionnez le forfait<span class="small text-danger"> *</span></label>
                                    {!! Form::select('forfait', $forfait->pluck('name', 'id'),null,['class' => 'form-control select2','required' => 'required', 'size' => '1'] ) !!}
                                </div>
                            </div>
                        </div>

                      
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label class="form-control-label">Selectionnez le moyen de paiement<span class="small text-danger"> </span></label></br>
                                <div class="custom-control custom-switch">
                                  <input type="radio" class="custom-control-input" id="carte" name="type" checked>
                                  <label class="custom-control-label" for="carte">Carte</label>
                                </div></br>
                                <div class="custom-control custom-switch">
                                  <input type="radio" class="custom-control-input" id="paypal" name="type">
                                  <label class="custom-control-label" for="paypal">Paypal</label>
                                </div>
                              </div>
                            </div>
                      </div>
                      
                      <div id="carte-content">
                          
                            <div class="row">
                                <div class="col-lg-12">
                                  <div class="form-group required">
                                    <label class="form-control-label">Nom sur la carte<span class="small text-danger"> *</span></label>
                                    <input type="text" class="form-control" size='4'>
                                  </div>
                                </div>
                           </div>
                          
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group cart required">
                                <label class="form-control-label">Numéro de la carte<span class="small text-danger"> *</span></label>
                                <input type="text" class="form-control card-number" autocomplete='off' size='20'>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-4">
                              <div class="form-group cvc required">
                                <label class="form-control-label">CVV<span class="small text-danger"> *</span></label>
                                <input type="text" class="form-control card-cvc" autocomplete='off' placeholder='ex. 311' size='4'>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="form-group expiration required">
                                <label class="form-control-label">Expiration<span class="small text-danger"> *</span></label>
                                <input type="text" class="form-control card-expiry-month"  placeholder='MM' size='2'>
                              </div>
                            </div>
                            <div class="col-lg-4">
                              <div class="form-group expiration required">
                                <label class="form-control-label"><span class="small text-danger"> *</span></label>
                                <input type="text" class="form-control card-expiry-year"  placeholder='YYYY' size='4'>
                              </div>
                            </div>
                        </div>
                     </div>
                      
                      <div id="paypal-content" style="display:none">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group cart">
                                <label class="form-control-label">Numéro Paypal<span class="small text-danger"> *</span></label>
                                <input type="text" class="form-control"  size='20' name="paypal"/>
                              </div>
                            </div>
                          </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group total">
                            <label class="form-control-label">Montant à payer pour ce forfait</label>
                            <input type="text" class="form-control" disabled name="price">
                          </div>
                        </div>
                      </div>

                    </div>
                                        
                    <div class="pl-lg-4">
                      <div class="row">
                        <div class="col text-center">
                          <button type="submit" class="btn btn-primary submit-button" style="margin-top: 10px;">Souscrire</button>
                        </div>
                      </div>
                    </div>
                    <br/><br/>
                    <div class='form-row'>
                      <div class='col-md-12 error form-group hide'>
                        <div class='alert-danger alert'>Merci de corriger les erreurs.</div>
                      </div>
                    </div>
                  </form>
                  @if ((Session::has('success-message')))
                  <div class="alert alert-success col-md-12">{{Session::get('success-message') }}</div>
                  @endif @if ((Session::has('fail-message')))
                    <div class="alert alert-danger col-md-12">{{Session::get('fail-message') }}</div>
                  @endif
                </div>
                <div class='col-md-2'></div>
              </div>
            </div>           
        </section>
    </div>
@endsection

@section('scripts')

<script src="https://code.jquery.com/jquery-1.12.3.min.js"
    integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="
    crossorigin="anonymous"></script>
  <script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
    integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
    crossorigin="anonymous"></script>

<script>
    $(function() {
        
        const forfait_id = $('[name="forfait"]').val()
        detailsForfait(forfait_id);
        
        $('[name="forfait"]').change(function(){
            const forfait_id = $(this).val()
            detailsForfait(forfait_id)
       })
       
       if($('#paypal').is(':checked'))
       {
           $('#carte-content').css('display', 'none')
           $('#paypal-content').css('display', '')
       }
       
       $('#carte').click(function(){
           $('#carte-content').css('display', '')
           $('#paypal-content').css('display', 'none')
       })
       
       $('#paypal').click(function(){
           
           $('#carte-content').css('display', 'none')
           $('#paypal-content').css('display', '')
       })
        
        $('form.require-validation').bind('submit', function(e) {
          var $form         = $(e.target).closest('form'),
              inputSelector = ['input[type=email]', 'input[type=password]',
                               'input[type=text]', 'input[type=file]',
                               'textarea'].join(', '),
              $inputs       = $form.find('.required').find(inputSelector),
              $errorMessage = $form.find('div.error'),
              valid         = true;
          $errorMessage.addClass('hide');
          $('.has-error').removeClass('has-error');
          $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
              $input.parent().addClass('has-error');
              $errorMessage.removeClass('hide');
              e.preventDefault(); // cancel on first error
            }
          });
        });
      });
      $(function() {
        var $form = $("#payment-form");
        $form.on('submit', function(e) {
          if (!$form.data('cc-on-file')) {
            e.preventDefault();
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
              number: $('.card-number').val(),
              cvc: $('.card-cvc').val(),
              exp_month: $('.card-expiry-month').val(),
              exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
          }
        });
        function stripeResponseHandler(status, response) {
          if (response.error) {
            $('.error')
              .removeClass('hide')
              .find('.alert')
              .text(response.error.message);
          } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.append("<input type='hidden' name='prix' value='" + $('[name="forfait_price"]').val() + "'/>");
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
          }
        }
      })
      
    function detailsForfait(id)
    {
      $('#detais-forfait, #elements-forfait').empty()
      $('#detais-forfait, #elements-forfait').parent().parent().css('display',  'none')
        var xhr = getXMLHttp();
        var url = "../forfaits/details/"+id
        
        xhr.onreadystatechange = function()    
            {
                if(xhr.readyState == 4)
                    {    
                        if(xhr.status == 200)
                        {
                                var info = xhr.responseText;
                                var json = eval('('+info+')');
                                //console.log(json.forfait.services[0].name)
                                placerServices(json.forfait)
                            

                        }
                    }     
            }
        
        xhr.open("GET", url , false);
        xhr.send(null);
    }

   function placerServices(json)
   {
      let data = ''

        $('#detais-forfait').parent().parent().css('display',  'block')
        $('#elements-forfait').parent().parent().css('display',  'block')
        $('<div class="row">\n\
            <div class="col-md-4">\n\
               <p><span class="section-title">Nom:</span><span class="font-weight-bold">'+json.name+'</span></p>\n\
            </div>\n\
            <div class="col-md-4">\n\
               <p><span class="section-title">Nombre de viste par mois:</span><span class="font-weight-bold">'+json.nbre_visite+'</span></p>\n\
            </div>\n\
            <div class="col-md-4">\n\
               <p><span class="section-title">Prix:</span><span class="font-weight-bold">'+json.price+'</span></p>\n\
            </div>\n\
        </div>').appendTo('#detais-forfait')
        
        $('[name="price"]').val(json.price)
        $('[name="forfait_price"]').val(json.price)

        for(var i = 0 ; i < json.services.length ; i++)
         {
            
            data += '<span class="font-weight-bold">'+json.services[i].name + '</span>;&nbsp'
         }

        $('<div class="row">\n\
                <div class="col-md-10 offset-md-1">\n\
                  <h3>Liste des services</h3>\n\
                <p>'+ data +'</p>\n\
                </div>\n\
        </div>').appendTo('#elements-forfait')
   }
</script>
    </script>


@endsection