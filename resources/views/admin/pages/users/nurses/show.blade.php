@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Medical')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Détails du patient</h1>
            </div>

            <div class="section-body">

               <!-- <div class="row">
                  <div class="col-md-4 col-12 offset-md-4">
                      @if($user->profile))
                        <img alt="image" width="100" height="100" src="{{ asset('assets/admin/uploads/'.$user->profile) }}" class="rounded-circle mr-1">
                      @else
                        <img alt="image" width="100" height="100" src="{{ asset('assets/admin/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
                      @endif
                  </div>
               </div> -->

               <div class="row">
                   <div class="col-md-6 col-12">
                      <div class="card">
                           <div class="card-body">
                               <p class="section-lead">
                                  <span class="section-title">Noms & Prenoms</span>: 
                                  <span class="font-weight-bold">{{ $user->name }} {{ $user->surname }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Adresse Email</span>: 
                                  <span class="font-weight-bold">{{ $user->email }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Profession</span>
                                  <span class="font-weight-bold">{{ $user->profession }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Ville ou Quartier</span>:
                                  <span class="font-weight-bold">{{ $user->localisation->name }}</span>
                                </p>
                           </div>
                      </div>
                   </div>
                   <div class="col-md-6 col-12">
                       <div class="card">
                          <div class="card-body">
                                <p class="section-lead">
                                  <span class="section-title">Téléphone</span>:
                                  <span class="font-weight-bold">{{ $user->contact ? $user->contact : '--'  }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Lieu Naissance</span>:
                                  <span class="font-weight-bold">{{ $user->lieuNaissance ? $user->lieuNaissance : '--' }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Age</span>:
                                  <span class="font-weight-bold">{{ $user->dateNaisance ? \Carbon\Carbon::parse($user->dateNaisance)->diff(\Carbon\Carbon::now())->format('%y ans, %m mois and %d jours') : '--' }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Profession</span>:
                                  <span class="font-weight-bold">{{ $user->profession }}</span>
                                </p>
                          </div>
                       </div>
                   </div>
               </div>
            </div>
        </section>
    </div>
@endsection