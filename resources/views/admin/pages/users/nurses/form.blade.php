<?php
if ($user->id) {
    $options = ['method' => 'put', 'url' => route('users.update',$user)];
} else {
    $options = ['method' => 'post', 'url' => route('users.nurses.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($user, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
    @csrf
    <div class="card">
        <div class="card-header">
            <h4>Ajouter un Utilisateur</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-12 offset-md-1">
        <div class="card">
            <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                            <div class="">
                                {!! Form::text('name', null, ['class' => 'form-control '.( $errors->has('name') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your name
                                    @error('name')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('surname', 'Prenom', ['class' => 'control-label']) !!}
                            <div class="">
                                {!! Form::text('surname', null, ['class' => 'form-control '.( $errors->has('surname') ? 'is-invalid' :''), 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your surname
                                    @error('surname')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('contact', 'Telephone', ['class' => 'control-label']) !!}
                            <div class="">
                                {!! Form::text('contact', null, ['class' => 'form-control '.( $errors->has('contact') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your email
                                    @error('contact')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('email', 'Email', ['class' => 'col-sm-7 control-label']) !!}
                            <div class="">
                                {!! Form::text('email', null, ['class' => 'form-control '.( $errors->has('email') ? 'is-invalid' :''), 'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your email
                                    @error('email')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('password', 'Mot de passe', ['class' => 'control-label']) !!}
                            <div class="">
                            {!! Form::text('password', null, ['class' => 'form-control '.( $errors->has('password') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"2", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your password
                                    @error('password')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('password_confirmation', 'Confimez votre mot de passe', ['class' => 'control-label']) !!}
                            <div class="">
                                {!! Form::text('password_confirmation', null, ['class' => 'form-control '.( $errors->has('password_confirmation') ? 'is-invalid' :''),'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                                <div class="invalid-feedback">
                                    Please fill in your password confirmmation
                                    @error('password_confirmation')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                             <input type="hidden" name="nurses" value="1" />
                             <input type="hidden" name="role" value="medecin" />
                             <input type="hidden" name="rule" value="patient" />
                             <input type="hidden" name="patient" value="0" />
                        </div>
                        <div class="form-group col-md-6">
                            <div class="card-footer text-right">
                                <button class="btn btn-secondary" type="reset">Reset</button>
                                <button class="btn btn-primary" type="submit" name="create">Validé</button>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        </div>
    </div>

{!! Form::close() !!} 
