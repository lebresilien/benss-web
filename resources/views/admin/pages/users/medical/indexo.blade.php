@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Medical')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dossier Medical</h1>
            </div>

            <div class="section-body">
               <div class="row">
                   <div class="col-md-6 col-12">
                      <div class="card">
                           <div class="card-body">
                               <p class="section-lead">
                                  <span class="section-title">Noms & Prenoms</span>: 
                                  <span class="font-weight-bold">{{ $user->name }} {{ $user->surname }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Adresse Email</span>: 
                                  <span class="font-weight-bold">{{ $user->email }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Profession</span>:
                                  <span class="font-weight-bold">{{ $user->profession }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Ville ou Quartier</span>:
                                  <span class="font-weight-bold">{{ $user->localisation->name }}</span>
                                </p>
                           </div>
                      </div>
                   </div>
                   <div class="col-md-6 col-12">
                       <div class="card">
                          <div class="card-body">
                                <p class="section-lead">
                                  <span class="section-title">Téléphone</span>:
                                  <span class="font-weight-bold">{{ $user->contact ? $user->contact : '--'  }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Lieu Naissance</span>:
                                  <span class="font-weight-bold">{{ $user->lieuNaissance ? $user->lieuNaissance : '--' }}</span>
                                </p>
                                <p class="section-lead">
                                  <span class="section-title">Age</span>:
                                  <span class="font-weight-bold">{{ $user->dateNaisance ? \Carbon\Carbon::parse($user->dateNaisance)->diff(\Carbon\Carbon::now())->format('%y ans, %m mois and %d jours') : '--' }}</span>
                                </p>
                               
                          </div>
                       </div>
                   </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       <div class="col-sm-8 offset-sm-2">
                          <h3>Forfait et services du patient</h3>
                       </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       <div class="col-sm-8 offset-sm-2">
                          <div class="row">
                             <div class="col-sm-4 ">
                                <p class="section-title">
                                  <span class="">{{ $forfait->name }}</span>
                                </p>
                             </div>
                             <div class="col-sm-8 p-2">
                                <h5>Services</h5>
                                <ul class="list-group list-group-flush">
                                  @forelse($forfait->services as $service)
                                    <li class="list-group-item">{{ $service->name }}</li>
                                  @empty
                                  @endforelse
                                </ul>
                             </div>
                          </div>
                       </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       <div class="col-sm-8 offset-sm-2">
                          <h3>Les autres forfaits et services</h3>
                       </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       <div class="col-sm-8 offset-sm-2">
                          <div class="row">
                             <div class="col-sm-4 ">
                                <p class="section-title">
                                  <span class="">{{ $aute_forfaits->name }}</span>
                                </p>
                             </div>
                             <div class="col-sm-8 p-2">
                                <h5>Services</h5>
                                <ul class="list-group list-group-flush">
                                  @forelse($aute_forfaits->services as $service)
                                    <li class="list-group-item">{{ $service->name }}</li>
                                  @empty
                                  @endforelse
                                </ul>
                             </div>
                          </div>
                       </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       <div class="col-sm-8 offset-sm-2">
                          <h3>Liste des interventions</h3>
                       </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                     <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                                <th class="text-center">
                                #
                                </th>
                                <th>Nom Medecin</th>
                                <th>Nom Infirmier</th>
                                <th>Symptomes/Forfait</th>
                                <th>Date && Heure</th>
                                <th>Action</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($rdvs as $rdv)
                                <tr>
                                  <td>{{ $loop->iteration }}</td>
                                  <td>{{  $rdv->doctor->name }} {{  $rdv->doctor->surname }} </td> 
                                  <td>{{  $rdv->nurse->name }} {{  $rdv->nurse->surname }} </td>
                                  <td>{{ $rdv->symptomes ? $rdv->symptomes : $rdv->forfait->name }}</td>
                                  <td>{{ $rdv->date_rdv }} </td>
                                  <td>
                                      <a class="btn btn-info btn-action mr-1" href="{{ route('soins.details',$rdv->id) }}" data-toggle="tooltip" title="" data-original-title="Show">
                                            <i class="fas fa-eye"></i>
                                      </a>
                                  </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                     </div>
                  </div>
               </div>
            </div>
        </section>
    </div>
@endsection