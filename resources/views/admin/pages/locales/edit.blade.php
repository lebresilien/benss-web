@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Categories_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Modifier la zone</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.locales.form')
            </div>
        </section>
    </div>
@endsection