<?php
if ($localisation->id) {
    $options = ['method' => 'put', 'url' => route('locales.update',$localisation)];
} else {
    $options = ['method' => 'post', 'url' => route('locales.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($localisation, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf


<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom de la zone') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Nom de la zone..."]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
