@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Zone_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 style="text-align:center" class="text-uppercase">Détails de l'intervention</h1>
            </div>

            <div class="section-body">
                <div class="section-body">
                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">Informations du patient</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body">
                                    
                                    <p class="section-title"> Noms et Prenoms:  {{ $rdv->user->name }}  {{ $rdv->user->surname }}</p>
                                    <p class="section-title"> Taille:  {{ $rdv->taille }} m </p>
                                    <p class="section-title">  Poids: {{ $rdv->poids }} Kg</p>
                                    <p class="section-title">  Temperature: {{ $rdv->temperature }} °c </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">Raison de l'intervention</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->service->name }} 
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">Nom du medecin</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->doctor->name }}  {{ $rdv->doctor->surname }}
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">Nom de l'infirmier</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->nurse->name }}  {{ $rdv->nurse->surname }}
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">liste des observations</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->observation }} 
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 offset-2"><h3 style="text-align:center">Ordonnance</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->ordonnance }} 
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                       <div class="col-sm-8 offset-2"><h3 style="text-align:center">Date de l'intervention</h3></div>
                        <div class="col-12">
                            <div class="card">
                            
                                <div class="card-body" style="text-align:center">
                                    
                                       {{ $rdv->created_at}}
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection