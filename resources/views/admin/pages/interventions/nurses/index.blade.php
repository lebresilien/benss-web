@extends('admin.layouts.app')
@section('title', 'Interventions non effectuées par une infirmiere')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Intervention(s) non effectuée(s) par l'infirmière</h1>
            </div>

            <div class="section-body">
            	<div class="row">
            		<div class="col-12">
            			<div class="card">
            				<div class="card-header"></div>
            				<div class="card-body">
            					<div class="table-responsive">
            						<table class="table table-striped table-bordered">
            							<thead class="">
                            				<tr>
                                				<th>Nom Patient</th>
                                				<th>Nom Medecin</th>
                                				<th>Service</th>
                                				<th>Date intervention</th>
                                				
                            				</tr>
                            			</thead>
                            			<tbody>
            								@forelse($rdvs as $rdv)
            									<tr>
            										<td>{{$rdv->user->name}} {{$rdv->user->surname}}</td>
            										<td>{{$rdv->doctor->name}} {{$rdv->doctor->surname}}</td>
            										<td>{{$rdv->symptomes ? $rdv->symptomes : $rdv->service->name }}</td>
            										<td>{{$rdv->updated_at}}</td>
            										
            									</tr>
                                                @empty
            								@endforelse
            							</tbody>
            						</table>
            					</div>
            				</div>
            				<div class="card-footer"></div>
            			</div>
            		</div>
            	</div>
            </div>
        </section>
    </div>
@endsection