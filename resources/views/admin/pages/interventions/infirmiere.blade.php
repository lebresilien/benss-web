@extends('admin.layouts.app')
@section('title', 'Interventions non effectuées par une infirmiere')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Intervention(s) non effectuée(s) par l'infirmière</h1>
            </div>

            <div class="section-body">
            	<div class="row">
            		<div class="col-12">
            			<div class="card">
            				<div class="card-header"></div>
            				<div class="card-body">
            					<div class="table-responsive">
            						<table class="table table-striped table-bordered">
            							<thead class="">
                            				<tr>
                                				<th>Nom</th>
                                				<th>Elément</th>
                                				<th>Date de l'intervention</th>
                                				<th>Patient</th>
                                				<th>Soin</th>
                            				</tr>
                            			</thead>
                            			<tbody>
            								@foreach($query as $quer)
            									<tr>
            										<td>{{$quer->name}}</td>
            										<td>{{$quer->element}}</td>
            										<td>{{$quer->date_inter}}</td>
            										<td>{{$quer->user_id}}</td>
            										<td>{{$quer->soin_id}}</td>
            									</tr>
            								@endforeach
            							</tbody>
            						</table>
            					</div>
            				</div>
            				<div class="card-footer"></div>
            			</div>
            		</div>
            	</div>
            </div>
        </section>
    </div>
@endsection