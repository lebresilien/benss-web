@extends('admin.pages.auth.layouts.app')
@section('title', 'Confirmation')


@section('content')

<div class="col-sm-8 offset-2 col-12">
      
    <div class="card card-primary" style="margin-top:200px">
    <div class="section-body">
      <h2 class="section-title">Confirmation</h2>
      <p class="section-lead">
         Un mail de validation a été envoyé à votre adresse email
      </p>
      
    </div>
    </div>
    
</div>
@endsection