
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title')</title>
    @include('admin.layouts.partials.include')
    @stack('base_link')
</head>

<body style="background:#FFFFFF">
  <div id="app">
    <section class="section">
      <div class="container-fluid mt-3" style="background:#FFFFFF">
        <div class="col-sm-10 offset-sm-1">
          <div class="row">
             @yield('content')
          </div>
        <div>
      </div>
    </section> 
  </div>

  @stack('base_script')
</body>
</html>
