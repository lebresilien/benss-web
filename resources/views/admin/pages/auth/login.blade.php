@extends('admin.pages.auth.layouts.app')
@section('title', 'Login')
@section('content')

<div class="col-md-6 col-12">
    <img src="{{ asset('public/assets/images/boy.png')}}" class="img-fluid" height="100" />
</div>

<div class="col-md-6 col-12 mt-5 pt-5">  
      {!! Form::open(['url' => 'login', 'class'=>'needs-validation', 'novalidate'=>'']) !!}
      
                        @if ($errors->any())
                             @foreach ($errors->all() as $error)
                                 <div class="alert alert-danger" role="alert">{{$error}}</div>
                             @endforeach
                         @endif
     
                        <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::email('email', null, ['class' => 'form-control '.( $errors->has('email')), 'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your email
                        </div>
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                  <div class="form-group">
                    <div class="d-block">
                         {!! Form::label('password', 'Mot de passe', ['class' => 'control-label']) !!}
                      <div class="float-right">
                        <a href="{{ route('admin.send_email') }}" class="text-small">
                          Mot de passe oublié ?
                        </a>
                      </div>
                    </div>
                    {!! Form::password('password', ['class' => 'form-control '.( $errors->has('password')), 'required' => 'required', 'tabindex'=>"2"] ) !!}
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                    @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      {!! Form::checkbox("remember", null, old('remember') ? 'checked' : '', ['class' => 'custom-control-input', 'tabindex' =>'3', 'id' =>'remember']) !!}
                      {!! Form::label('remember', 'Se souvenir de moi', ['class' => 'custom-control-label']) !!}
                    </div>
                  </div>

                  <div class="form-group">
                    {!! Form::submit('Login', ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex'=>'4']) !!}
                  </div>
    {!! Form::close() !!}

                <div class="mt-5 text-muted text-center">
                    Vous n'avez pas encore de compte ? <a href="{{ route('register') }}">Créez un compte</a>
                </div>
</div>
@endsection