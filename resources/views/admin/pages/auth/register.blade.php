@extends('admin.pages.auth.layouts.app')
@section('title', 'Inscription')


@section('content')
<div class="col-md-6 col-12">
    <img src="{{ asset('public/assets/images/boy.png')}}" class="img-fluid" height="100" />
</div>
<div class="col-md-6 col-12">
  <div class="card card-primary">
    <div class="card-header"><h4>Inscription</h4></div>
    <div class="card-body">
      {!! Form::open(['url' => 'register', 'class'=>'needs-validation', 'novalidate'=>'']) !!}
            @csrf
            <div class="form-group">
              <label for="name">Nom</label>
              <input id="name" type="text"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Entrez votre nom" name="name" tabindex="1" placeholder="Nom" value="{{ old('name') }}" autofocus required>
              <div class="invalid-feedback">
                {{ $errors->first('name') }}
              </div>
            </div>

            <div class="form-group">
              <label for="surname">Prenom</label>
              <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" placeholder="Entrez votre prenom" name="surname" tabindex="1" placeholder="Prénom" value="{{ old('surname') }}" autofocus>
              <div class="invalid-feedback">
                {{ $errors->first('surname') }}
              </div>
            </div>

            <div class="form-group">
              <label for="contact">Contact</label>
              <input id="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" placeholder="Entrez votre contact" name="contact" tabindex="1" placeholder="Numero de téléphone" value="{{ old('contact') }}" autofocus required>
              <div class="invalid-feedback">
                {{ $errors->first('contact') }}
              </div>
            </div>

            <div class="form-group">
              <label for="profession">Profession</label>
              <input id="profession" type="text" class="form-control{{ $errors->has('profession') ? ' is-invalid' : '' }}" placeholder="Entrez votre profession" name="profession" tabindex="1" placeholder="Proféssion" value="{{ old('profession') }}" autofocus required>
              <div class="invalid-feedback">
                {{ $errors->first('profession') }}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('localisation_id', 'Adresse') !!}
              {!! Form::select('localisation_id', $locales->pluck('name', 'id'), null, ['class' => 'select2', "data-height"=>"100%", "data-width"=>"100%"]); !!}
            </div>

          <div class="form-group">
            <label for="email">Email</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Adresse email " name="email" tabindex="1" value="{{ old('email') }}" autofocus required>
            <div class="invalid-feedback">
              {{ $errors->first('email') }}
            </div>
          </div>

          <div class="form-group">
            <label for="password" class="control-label">Mot de passe</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid': '' }}" placeholder="Entrez votre mot de passe" name="password" tabindex="2" required>
            <div class="invalid-feedback">
              {{ $errors->first('password') }}
            </div>
          </div>

          <div class="form-group">
            <label for="password_confirmation" class="control-label">Confirmer le mot de passe</label>
            <input id="password_confirmation" type="password" placeholder="Confirmez votre mot de passe" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid': '' }}" name="password_confirmation" tabindex="2" required>
            <div class="invalid-feedback">
              {{ $errors->first('password_confirmation') }}
            </div>
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
              Enregistrer
            </button>
          </div>
        {!! Form::close() !!}
    </div>
  </div>
  <div class="mt-5 text-muted text-center">
    Vous avez deja un compte ? <a href="{{ route('admin.login') }}">Connectez-vous</a>
  </div>
</div>
@endsection