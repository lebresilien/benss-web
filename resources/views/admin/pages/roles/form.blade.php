<?php
if ($role->id) {
    $options = ['method' => 'put', 'url' => route('roles.update',$role)];
} else {
    $options = ['method' => 'post', 'url' => route('roles.store')];
}
?>
@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($role, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf
<div class="card">
    <div class="card-header">
        <h4>Ajouter un Role</h4>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Name or title of the role..."]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
