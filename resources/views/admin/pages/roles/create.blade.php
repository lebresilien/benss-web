@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Rdv_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Type Rdv List</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.roles.form')
            </div>
        </section>
    </div>
@endsection