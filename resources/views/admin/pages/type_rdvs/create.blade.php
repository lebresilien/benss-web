@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Roles_list')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Ajouter un type</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.type_rdvs.form')
            </div>
        </section>
    </div>
@endsection