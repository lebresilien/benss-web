@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Serives-List')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Modifier le service</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.services.form')
            </div>
        </section>
    </div>
@endsection