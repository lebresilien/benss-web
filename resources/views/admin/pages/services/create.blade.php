@extends('admin.layouts.app')
@section('title', 'Administration Dashboard-Services')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="text-uppercase">Ajouter un service</h1>
            </div>

            <div class="section-body">
               @include('admin.pages.services.form')
            </div>
        </section>
    </div>
@endsection