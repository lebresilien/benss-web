@extends('admin.layouts.app')
@section('title', 'Administration Dashboard')
@section('content')

<div class="main-content">
        <section class="section">
        	<div class="section-header">
            	<h1>Tableau de bord</h1>
          	</div>

          	<div class="section-body"> 
          	
          	@if((auth()->user()->user_id == NULL && auth()->user()->role == 'patient')  ||  (auth()->user()->forfait_id == NULL && auth()->user()->role == 'patient') )
          	   <div class="row">
          	       <div class="col-sm-12">
    			        <div class="alert alert-warning" role="alert">
                             Vous ne pouvez pas effectuer de Rendez-vous car aucun medecin ne vous a encore été attribué. Et vous devez souscrire à un forfait avant.
                        </div>
        			 
          	       </div>
          	   </div>
          	@endif
          	   
          	   
          		<div class="row">
          		    
          		   
          			@if(auth()->user()->role == 'patient')
	          			<div class="col-xl-3 col-md-6 mb-4">
					        <div class="card border-left-primary shadow h-100 py-2">
					            <div class="card-body">
					                <div class="row no-gutters align-items-center">
					                    <div class="col mr-2">
					                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('Rendez-vous récents')}}</div>
					                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$rdvRecents}}</div>
					                    </div>
					                    <div class="col-auto">
					                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
					                    </div>
					                </div>
					                <a href="{{ route('rdvs.patient', Auth::user()->id) }}" class="btn btn-primary" target="_blank">En savoir plus</a>
					            </div>
					        </div>
	        			</div>
	        			
	        			
        			@endif

        			@if(auth()->user()->role == 'infirmier')          
		        		<div class="col-xl-3 col-md-6 mb-4">
					        <div class="card border-left-info shadow h-100 py-2">
					            <div class="card-body">
					                <div class="row no-gutters align-items-center">
					                    <div class="col mr-2">
					                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">{{__('Rendez-vous récents non validés')}}</div>
					                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$rdvRecentsNonValidesParInfirmiere}}</div>
					                    </div>
					                    <div class="col-auto">
					                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
					                    </div>
					                </div>
					                <a href="{{ route('rdvs.nurses.invalid') }}" class="btn btn-info" target="_blank">En savoir plus</a>
					            </div>
					        </div>
			        	</div>
			        	<div class="col-xl-3 col-md-6 mb-4">
					        <div class="card border-left-success shadow h-100 py-2">
					            <div class="card-body">
					                <div class="row no-gutters align-items-center">
					                    <div class="col mr-2">
					                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{ __('Intervention(s) non effectuée(s)')}}</div>
					                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$interventionsNonEffectueesParInfimiere}}</div>
					                    </div>
					                    <div class="col-auto">
					                    	<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
					                   	</div>
					                </div>
					                <a href="{{ route('interventions.nurse') }}" class="btn btn-success" target="_blank">En savoir plus</a>
					            </div>
					        </div>
		        		</div>
		        		<div class="col-xl-3 col-md-6 mb-4">
				            <div class="card border-left-warning shadow h-100 py-2">
				                <div class="card-body">
				                    <div class="row no-gutters align-items-center">
				                        <div class="col mr-2">
				                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">{{ __('Soins à administrer') }}</div>
				                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$soinsAAdministrerPourUneInfirmiere}}</div>
				                        </div>
				                        <div class="col-auto">
				                            <i class="fas fa-users fa-2x text-gray-300"></i>
				                        </div>
				                    </div>
				                    <a href="#" class="btn btn-warning" target="_blank">En savoir plus</a>
				                </div>
				            </div>
		        		</div>
	        		@endif
          		</div>
          		@if(auth()->user()->role == 'medecin')
          		    
    				<div class="row">
    						<div class="col-xl-3 col-md-6 mb-4">
    					        <div class="card border-left-success shadow h-100 py-2">
    					            <div class="card-body">
    					                <div class="row no-gutters align-items-center">
    					                    <div class="col mr-2">
    					                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{ __('Intervention(s) non effectuée(s)')}}</div>
    					                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$interventionsNonEffectueesParMedecin}}</div>
    					                    </div>
    					                    <div class="col-auto">
    					                    	<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
    					                   	</div>
    					                </div>
    					                <a href="{{ route('interventions.doctor') }}" class="btn btn-success" target="_blank">En savoir plus</a>
    					            </div>
    					        </div>
    		        		</div>
    				</div>	
    				
				@endif
          	</div>
        </section>
      </div>
@endsection