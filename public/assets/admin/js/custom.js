/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

// In your Javascript (external .js resource or <script> tag)
$(document).ready(function () {
    $('.select2').select2();
});

// DropzoneJS
if (window.Dropzone) {
    Dropzone.autoDiscover = false;
}

var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
"use strict";
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $("#bloquer_cours").on('click', () => {
        $("#bloquer_cours").addClass("d-none");
        $("#motif_style").removeClass('d-none');
    });
    $('#annulerBlocage').on('click', () => {
        $("#bloquer_cours").removeClass("d-none");
        $("#motif_style").addClass("d-none");
    });
});

