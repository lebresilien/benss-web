<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.pages.auth.login');
})->name('admin.login');

Route::get('/send-email', function (Request $request) {
    return view('admin.pages.auth.send_email');
})->name('admin.send_email');

Route::get('/inscription', function () {
    return view('admin.pages.auth.register');
})->name('inscription');

Route::get('/confirm', function () {
    return view('admin.pages.auth.confirm');
})->name('confirm');

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth'], 'prefix' => 'accueil'], function () {
   
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('locales', 'LocalisationController')->except(['show']);
    Route::resource('roles', 'RoleController')->except(['show']);
    Route::resource('type-rdvs', 'TypeRdvController')->except(['show']);
    Route::resource('antecedents', 'AntecedentController')->except(['show']);
    Route::resource('type-antecedents', 'TypeAntecedentController')->except(['show']);
    Route::resource('type-doctors', 'TypeDoctorController')->except(['show']);
    Route::resource('type-soins', 'TypeSoinController')->except(['show']);
    Route::resource('type-interventions', 'TypeInterventionController')->except(['show']);
    Route::post('users/password', 'UserController@password')->name('users.password');
    Route::get('users/password', 'UserController@getPassword')->name('users.password');
    Route::get('users/role', 'UserController@role');
    Route::put('users/profile', 'UserController@profile')->name('users.profile.update');
    Route::get('users/profile', 'UserController@getprofile')->name('users.profile');
    Route::get('users/zone/{id}/{role}', 'UserController@getUsersByZone')->name('users.zone');
    Route::get('users/listing/{type}', 'UserController@listing')->name('users.listing');
    Route::delete('users/delete/{id}', 'UserController@delete')->name('users.delete');
    Route::get('users/medical', 'UserController@dossierMedical')->name('users.medical');
    Route::get('users/medical/users/{id}', 'UserController@dossierMedicalBis')->name('users.medical.users'); 
    Route::get('users/nurses/index', 'UserController@nursePatient')->name('users.nurses.index');
    Route::get('users/nurses/create', 'UserController@createPatient')->name('users.nurses.create');
    Route::get('users/doctor/patient', 'UserController@doctorPatient')->name('users.doctor.patient');
    Route::get('users/forfait', 'UserController@userForfait')->name('users.forfait');
    Route::post('users/forfait/store', 'UserController@forfait')->name('users.forfait.store');
    Route::post('users/nurses/store', 'UserController@nurseStore')->name('users.nurses.store');
    Route::get('users/assign/patients', 'UserController@getAssignForm')->name('users.assign');
    Route::get('users/assign/{id}', 'UserController@assignFind')->name('users.assign.find');
    Route::post('users/assign/patients', 'UserController@assignPatientToDoctor')->name('users.assign.store');
    Route::resource('users', 'UserController');
    Route::get('intervention/nurse/', 'RdvController@interventionNonNurse')->name('interventions.nurse');
    Route::get('intervention/doctor/', 'RdvController@doctorInvalidRdv')->name('interventions.doctor');
    Route::get('rdvs/patient', 'RdvController@patientRdv')->name('rdvs.patient');
    Route::get('rdvs/doctor', 'RdvController@doctorRdv')->name('rdvs.doctors');
    Route::get('rdvs/nurse', 'RdvController@nurseRdv')->name('rdvs.nurses');
    Route::get('rdvs/nurse/invalid', 'RdvController@nurseInvalidRdv')->name('rdvs.nurses.invalid');
    Route::get('rdvs/confirm/{id}', 'RdvController@confirm');
    Route::resource('rdvs', 'RdvController');
    Route::get('soins/details/{id}', 'SoinController@details')->name('soins.details');
    Route::resource('soins', 'SoinController');
    Route::get('forfaits/details/{id}', 'ForfaitController@detailsForfait');
    Route::resource('forfaits', 'ForfaitController');
    Route::resource('services', 'ServiceController');

    // Route qui affiche les interventions non effectuées par un medecin
    Route::get('interventions/medecin/{id}', 'InterventionController@interventionsNonEffectueesParMedecin')->name('interventions.medecin');
    // Route qui affiche les interventions non effectuées par l'infirmière
    Route::get('interventions/infirmiere/{id}', 'InterventionController@interventionsNonEffectueesParInfirmiere')->name('interventions.infirmiere');
    // Route qui affiche les rendez-vous pour un patient
    //Route::get('rdvs/patient/{id}', 'RdvController@patientRDV')->name('rdvs.patient');

});
