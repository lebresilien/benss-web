<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
$api = app('Dingo\Api\Routing\Router');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'v1/auth','namespace' => 'App\Http\Controllers\API\v1\Auth'], function($route){  
        $route->post('/login', 'AuthController@login');
        $route->get('/', function(){ return response()->json('heloo');});
        $route->group(['middleware' => 'auth:api'], function($route) {
           $route->post('/user', 'AuthController@user');
           $route->post('/logout', 'AuthController@logout');
        });
   });
});

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'v1','namespace' => 'App\Http\Controllers\API\v1'], function($route){  
        $route->group(['middleware' => 'auth:api'], function($route) {
            $route->get('/rdvs', 'RdvController@create');
            $route->post('/rdvs', 'RdvController@store');
            $route->put('/rdvs/{id}', 'RdvController@update');
            $route->get('/rdvs/{id}/confirm', 'RdvController@confirm');
            $route->get('/rdvs/nurse', 'RdvController@nurseRdv');
            $route->get('/rdvs/nurse/invalid', 'RdvController@nurseInvalidRdv');
            $route->get('/rdvs/patient', 'RdvController@patientRdv');
            $route->get('/rdvs/doctor', 'RdvController@doctorInvalidRdv');
        });
       
   });
});

