<?php

namespace App\Mail;

use App\Models\Rdv;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RdvMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $rdv;
    public function __construct(Rdv $rdv)
    {
        $this->rdv = $rdv;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!$this->rdv->state)

            return $this->from('info@bensscameroun.com')
                        ->subject('Benss Cameroun - Demande de Rendez-vous')
                        ->view('admin.emails.rdvs.create')
                        ->with('user', $this->rdv);
        else

            return $this->from('info@bensscameroun.com')
                        ->subject('Benss Cameroun - Confirmation Rendez-vous')
                        ->view('admin.emails.rdvs.confirm')
                        ->with('user', $this->rdv);

    }
}
