<?php

namespace App\Policies;

use App\Models\Rdv;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RdvPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rdv  $rdv
     * @return mixed
     */
    public function view(User $user, Rdv $rdv)
    {
        return $user->id === $rdv->nurse_id ? Response::allow()
        : Response::deny('vous n\'avez pas les autorisations sur ce rdv.');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rdv  $rdv
     * @return mixed
     */
    public function update(User $user, Rdv $rdv)
    {
        return $user->id === $rdv->nurse_id ? Response::allow()
        : Response::deny('vous n\'avez pas les autorisations sur ce rdv.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rdv  $rdv
     * @return mixed
     */
    public function delete(User $user, Rdv $rdv)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rdv  $rdv
     * @return mixed
     */
    public function restore(User $user, Rdv $rdv)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rdv  $rdv
     * @return mixed
     */
    public function forceDelete(User $user, Rdv $rdv)
    {
        //
    }
}
