<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'surname', 'password', 'sexe', 'lieuNassance', 'dateNaissance', 'contact', 'profession', 'active',
         'forfait_id', 'doctor_id', 'localisation_id', 'activation_token', 'role', 'user_id', 'profile'
    ];
    protected $guard_name = 'web';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token', 'activation_token'
    ];


    public function forfait()
    {
      return $this->belongsTo('App\Models\Forfait');
    }

    public function type_doctor()
    {
      return $this->belongsTo('App\Models\TypeDoctor');
    }

    public function localisation()
    {
      return $this->belongsTo('App\Models\Localisation');
    }
}
