<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Antecedent extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'type_antecedent_id', 'user_id'
    ];

    public function type_antecedent()
    {
        return $this->belongsTo('App\Models\TypeAntecedent');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
