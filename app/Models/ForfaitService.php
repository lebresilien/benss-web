<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForfaitService extends Model
{

    protected $table = 'forfait_service';
    protected $fillable = [
        'forfait_id', 'service_id',  
    ];
}
