<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rdv extends Model
{
    protected $fillable = [
        'symptomes', 'user_id', 'doctor_id', 'nurse_id', 'type_soin_id', 'date_rdv', 
        'state', 'consult', 'forfait_id', 'observation', 'ordonnance', 'service_id',
         'poids', 'taille', 'temperature',
    ];

    public function type_soin()
    {
      return $this->belongsTo('App\Models\TypeSoin');
    }

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function nurse()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function doctor()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function forfait()
    {
      return $this->belongsTo('App\Models\Forfait');
    }

    public function service()
    {
      return $this->belongsTo('App\Models\Service');
    }


}
