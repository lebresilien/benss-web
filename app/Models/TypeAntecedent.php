<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeAntecedent extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];

    public function antecedents()
    {
        return $this->hasMany('App\Models\Antecedent');
    }
}
