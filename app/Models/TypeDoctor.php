<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDoctor extends Model
{
    protected $table = 'type_doctors';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function type_soins()
    {
      return $this->hasMany('App\Models\TypeSoin');
    } 
}
