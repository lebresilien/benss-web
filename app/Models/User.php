<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'surname', 'password', 'sexe', 'lieuNassance', 'dateNaissance', 'contact', 'profession', 'active', 'situation',
         'forfait_id', 'doctor_id', 'localisation_id', 'activation_token', 'role', 'user_id', 'profile', 'busy', 'email_verified_at',
    ];
    protected $guard_name = 'web';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token', 'activation_token'
    ];


    public function forfait()
    {
      return $this->belongsTo('App\Models\Forfait');
    }

    public function type_doctor()
    {
      return $this->belongsTo('App\Models\TypeDoctor');
    }

    public function localisation()
    {
      return $this->belongsTo('App\Models\Localisation');
    }

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function interventions()
    {
      return $this->hasMany('App\Models\Intervention');
    }

    public function antecedents()
    {
      return $this->hasMany('App\Models\Antecedent');
    }
}
