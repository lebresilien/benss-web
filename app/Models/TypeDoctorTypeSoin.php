<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeDoctorTypeSoin extends Model
{
    protected $table = 'type_doctor_type_soin';
    protected $fillable = [
        'type_doctor_id','type_soin_id',
    ];

    public $timestamps = false;
}
