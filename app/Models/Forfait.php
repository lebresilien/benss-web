<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forfait extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'price', 'nbre_visite', 'state',
    ];

    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }
}
