<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeRdv extends Model
{
    protected $table = 'type_rdvs';
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];
}
