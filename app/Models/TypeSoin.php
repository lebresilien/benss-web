<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeSoin extends Model
{
    protected $table = 'type_soins';
    public $timestamps = false;

    protected $fillable = [
        'name','type_doctor_id', 'price'
    ];

    public function type_doctor()
    {
      return $this->belongsTo('App\Models\TypeDoctor');
    }

    public function rdv()
    {
      return $this->hasMany('App\Models\Rdv');
    }


}
