<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function forfaits()
    {
        return $this->belongsToMany('App\Models\Forfait');
    }

    public function rdvs()
    {
        return $this->hasMany('App\Models\Rdv');
    }

}
