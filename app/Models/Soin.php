<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Soin extends Model
{
    protected $fillable = [
        'rdv_id', 'treatment', 'created_at', 'updated_at', 'user_id'
    ];

    public function rdv()
    {
      return $this->belongsTo('App\Models\Rdv');
    }

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }


    public function interventions()
    {
      return $this->hasMany('App\Models\Interventions');
    }
}
