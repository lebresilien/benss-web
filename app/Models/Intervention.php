<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Intervention extends Model
{
    protected $fillable = [
        'soin_id', 'user_id', 'name', 'element', 'date_inter','nurse_id', 'doctor_id', 'state'
    ];

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function soin()
    {
      return $this->belongsTo('App\Models\Soin');
    }
}
