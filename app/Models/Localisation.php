<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localisation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name','busy'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
 
}
