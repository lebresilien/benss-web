<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Localisation;

class InterventionController extends Controller
{
    // Fonction qui permet d'afficher les interventions non effectuées par un medecin
    public function interventionsNonEffectueesParMedecin($id){
    	$user = Auth::user()->id;
    	$query = DB::table('interventions')->select('name', 'element', 'date_inter', 'user_id', 'soin_id')->where('doctor_id', $user)->where('state', 0)->get();
    	return view('admin.pages.interventions.medecin', compact('query', 'user'));
    }

    // Fonction qui permet d'afficher les interventions non effectuées par l'infirmière
    public function interventionsNonEffectueesParInfirmiere(){
    	$user = Auth::user()->id;
    	$query = DB::table('interventions')->select('name', 'element', 'date_inter', 'user_id', 'soin_id')->where('nurse_id', $user)->where('state', 0)->get();
    	return view('admin.pages.interventions.infirmiere', compact('query', 'user'));
    }
}
