<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rdv;
use App\Models\Intervention;
use App\Models\Soin;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         // Compter les rendez-vous récents d'un patient donné
         $rdvRecents = Rdv::where('user_id', Auth::user()->id)->count();
         // Compter les rendez-vous récents non validés pour une infirmière donnée
         $rdvRecentsNonValidesParInfirmiere = Rdv::where('nurse_id', Auth::user()->id)->where('state', 0)->count();
         // Compter les interventions non effectuées auxquelles l'infirmière doit participer
         
         $interventionsNonEffectueesParInfimiere = Rdv::where('nurse_id', Auth::user()->id)->where('state', true)->where('consult',false)->count();
         // Compter les soins à administrer pour une infimière
         $soinsAAdministrerPourUneInfirmiere = Soin::where('user_id', Auth::user()->id)->count();
         // Compter les interventions non efectuées auxquelles le médecin doit participer
         $interventionsNonEffectueesParMedecin = Rdv::where('doctor_id', Auth::user()->id)->where('state', true)->count();
         
         return view('admin.index', compact('rdvRecents', 'rdvRecentsNonValidesParInfirmiere', 'interventionsNonEffectueesParInfimiere', 'soinsAAdministrerPourUneInfirmiere', 'interventionsNonEffectueesParMedecin')); 
    }
}
