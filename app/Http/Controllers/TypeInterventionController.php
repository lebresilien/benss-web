<?php

namespace App\Http\Controllers;

use App\Models\TypeIntervention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TypeInterventionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeIntervention::all();
        return view('admin.pages.type_interventions.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new TypeIntervention();
        return view('admin.pages.type_interventions.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_interventions'],
            ]
        )->validate();

        $type = TypeIntervention::create(["name" => $request->name]);
        return redirect()->route('type-interventions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeIntervention  $tYpeIntervention
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeIntervention  $tYpeIntervention
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = TypeIntervention::findOrFail($id);
        return view('admin.pages.type_interventions.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeIntervention  $tYpeIntervention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = TypeIntervention::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_interventions'],
            ]
        )->validate();

        $type->update(["name" => $request->name]);
        return redirect()->route('type-interventions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeIntervention  $tYpeIntervention
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = TypeIntervention::findOrFail($id);
        $type->delete();
        return redirect()->route('type-interventions.index');
    }
}
