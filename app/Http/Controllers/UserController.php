<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TypeDoctor;
use App\Mail\UserMail;
use App\Models\Localisation;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\PermissionRegistrar;
use Session;
use Redirect;
/*
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
*/

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('localisation')->get();
        return view('admin.pages.users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name', '!=', 'patient')->get();
        $types = TypeDoctor::all();
        $locales = Localisation::all();
        $user = new User();
        return view('admin.pages.users.create', compact('locales', 'roles', 'user', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255','nullable'],
                'contact' => ['required', 'string', 'max:255'],
                'patients' => ['required', 'sometimes'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        )->validate();

        $nurse_id = $request->nurses;

        if($nurse_id == 0 && $request->role == "medecin" && empty($request->patients))
        {
            /* if(empty($request->patients))
            return back()->with('error', 'veuillez rajouter aux moins un patient'); */
            /* Validator::make(
                $request->all(),
                [
                    'namef' => ['required', 'string', 'max:255'],
                    'surnamef' => ['required', 'max:255', 'string', 'nullable'],
                    'contactf' => ['required', 'max:255', 'string'],
                    'emailf' => ['required', 'email', 'max:255'],
                    'passwordf' => ['required', 'string', 'min:8', 'required_with:password_confirmationf', 'same:password_confirmationf'],
                    'password_confirmationf' => ['min:8']
                ]
            )->validate(); */

            try{

                DB::beginTransaction();

                // creation du medecin
                $doctor = User::create(["name" => $request->name, 'email' => $request->email, 'password' =>$request->password,
                "surname" => $request->surname,"contact" => $request->contact,"localisation_id" => $request->zone,
                 "role" => $request->role, "type_doctor_id" => $request->type, 'email_verified_at' => now() ]);

                // liaison du medecin aux patients
                /* foreach($request->patients as $el){
                    $patient = \App\Models\User::findOrFail($el);
                    $patient->user_id = $doctor->id;
                    $patient->busy = true;
                    $patient->save();
                } */
               
                
                // creation de  l'infirmier
                /*$nurse = User::create(["name" => $request->namef, 'email' => $request->emailf, 'password' => Hash::make($request->passwordf),
                "surname" => $request->surnamef,"contact" => $request->contactf,"localisation_id" => $request->zone,
                 "role" => "infirmier", "user_id" => $doctor->id, 'email_verified_at' => now() ]);*/

                // attribution du role
                /* $role = Role::where('name', $request->role)->first();
                $doctor->assignRole([$role->id]); */

                // attribution du role
               /*  $role = Role::where('name', 'infirmier')->first();
                $nurse->assignRole([$role->id]); */

                // envoi de mail audoctoeur et à l'infirmier
                
                Mail::to($doctor->email)->queue(new UserMail($doctor));
                $doctor->password = Hash::make($request->password);
                $doctor->save();
                
               // Mail::to($nurse->email)->queue(new UserMail($nurse));

                DB::commit();

            }catch (\PDOException $e) {
                return $e->getMessage();
                DB::rollBack();
            } 

            return redirect()->route('users.index')->with('store');
           
        }

        if($nurse_id != 0 && $request->role == "medecin"  && empty($request->patients))
        {
            try{
                  DB::beginTransaction();

                    $doctor = User::create(["name" => $request->name, 'email' => $request->email, 'password' => $request->password,
                    "surname" => $request->surname,"contact" => $request->contact,"localisation_id" => $request->zone,
                    "role" => $request->role, "type_doctor_id" => $request->type, 'email_verified_at' => now() ]); 
                
                    // liaison du medecin à l'infirmier
                    $nurse = User::where('id', $request->nurses)->first();
                    $nurse->user_id = $doctor->id;
                    $nurse->save();

                    // attribution du role et envoi du mail
                    $role = Role::where('name', $request->role)->first();
                    $doctor->assignRole($role->id);
                    
                    Mail::to($doctor->email)->queue(new UserMail($doctor));

                    $doctor->password = Hash::make($request->password);
                    $doctor->save();
                  DB::commit();

                }catch (\PDOException $e) {
                    return $e->getMessage();
                    DB::rollBack();
                }
            
                return redirect()->route('users.index')->with('store');
        }

        if($nurse_id != 0 && $request->role == "medecin"  && !empty($request->patients))
        {
            try{
                  DB::beginTransaction();

                     $doctor = User::create(["name" => $request->name, 'email' => $request->email, 'password' => $request->password,
                     "surname" => $request->surname,"contact" => $request->contact,"localisation_id" => $request->zone,
                      "role" => $request->role, "type_doctor_id" => $request->type, 'email_verified_at' => now() ]); 
                 
                    // liaison du medecin à l'infirmier
                    $nurse = User::where('id', $request->nurses)->first();
                    $nurse->user_id = $doctor->id;
                    $nurse->save();

                    // liaison du medecin aux patients
                    foreach($request->patients as $el){
                        $patient = \App\Models\User::findOrFail($el);
                        $patient->user_id = $doctor->id;
                        $patient->busy = true;
                        $patient->save();
                    } 

                     // attribution du role et envoi du mail
                     $role = Role::where('name', $request->role)->first();
                     $doctor->assignRole($role->id);
                     
                     Mail::to($doctor->email)->queue(new UserMail($doctor));
                     $doctor->password = Hash::make($request->password);
                     $doctor->save();
                  DB::commit();

                }catch (\PDOException $e) {
                    return $e->getMessage();
                    DB::rollBack();
                }
            
                return redirect()->route('users.index')->with('store');
        }

        if($nurse_id == 0 && $request->role == "medecin" && !empty($request->patients))
        {
           
            try{

                DB::beginTransaction();

                // creation du medecin
                $doctor = User::create(["name" => $request->name, 'email' => $request->email, 'password' => $request->password,
                "surname" => $request->surname,"contact" => $request->contact,"localisation_id" => $request->zone,
                 "role" => $request->role, "type_doctor_id" => $request->type, 'email_verified_at' => now() ]);

                // liaison du medecin aux patients
                foreach($request->patients as $el){
                    $patient = \App\Models\User::findOrFail($el);
                    $patient->user_id = $doctor->id;
                    $patient->busy = true;
                    $patient->save();
                } 

                // envoi de mail audoctoeur et à l'infirmier
                
                Mail::to($doctor->email)->queue(new UserMail($doctor));
                $doctor->password = Hash::make($request->password);
                $doctor->save();

                DB::commit();

            }catch (\PDOException $e) {
                return $e->getMessage();
                DB::rollBack();
            } 

            return redirect()->route('users.index')->with('store');
           
        }


        if($request->role == "user")
        {
            try{
                DB::beginTransaction();

                   $user = User::create(["name" => $request->name, 'email' => $request->email, 'password' => $request->password,
                   "surname" => $request->surname,"contact" => $request->contact, "role" => $request->role, 'email_verified_at' => now()]); 

                   // attribution du role et envoi du mail
                   $role = Role::where('name', $request->role)->first();
                   $user->assignRole([$role->id]);
                   $user->password = $request->password;
                   Mail::to($user->email)->queue(new UserMail($user));
                   $user->password = Hash::make($request->password);
                   $user->save();
                DB::commit();

              }catch (\PDOException $e) {
                  return $e->getMessage();
                  DB::rollBack();
              }
          
              return redirect()->route('users.index')->with('store');
        }


        if($request->role == "infirmier")
        {
            try{
                DB::beginTransaction();

                   $nurse = User::create(["name" => $request->name, 'email' => $request->email, "user_id" => $request->doctor, 'password' => $request->password,
                   "surname" => $request->surname,"contact" => $request->contact, "localisation_id" => $request->zone, "role" => $request->role, 'email_verified_at' => now()]); 

                   // attribution du role et envoi du mail
                   $role = Role::where('name', $request->role)->first();
                   $nurse->assignRole($role->id);
                   
                   Mail::to($nurse->email)->queue(new UserMail($nurse));
                   $nurse->password = Hash::make($request->password);
                   $nurse->save();
                DB::commit();

              }catch (\PDOException $e) {
                  return $e->getMessage();
                  DB::rollBack();
              }
          
              return redirect()->route('users.index')->with('store');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.pages.users.nurses.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.pages.users.nurses.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255','nullable'],
                'contact' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        )->validate();

         try{
             DB::beginTransaction();


                $user->update(["name" => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password),
                "surname" => $request->surname,"contact" => $request->contact]); 
            
             DB::commit();

           }catch (\PDOException $e) {
               return $e->getMessage();
               DB::rollBack();
           }
       
           return redirect()->route('users.nurses.index')->with('update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.index')->with('delete');
    }

    public function delete(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('users.listing', ['type' => $request->type])->with('delete');
    }

    public function getUsersByZone($id, $role){

        if($role == "medecin")
        {
            $patients = \App\Models\Localisation::with(['users' => function($req){
                $req->where('role', 'patient')->where('busy', false);
            }])->where('id', $id)->get();
    
            $nurses = \App\Models\Localisation::with(['users' => function($req){
                $req->where('role', 'infirmier');
            }])->where('id', $id)->get();
           
            $patients = $patients->pluck('users');
            $nurses = $nurses->pluck('users');
    
    
            return response()->json(['patients' => $patients, 'nurses' => $nurses]);
        }
        else
        {
            $doctors = \App\Models\Localisation::with(['users' => function($req){
                $req->where('role', 'medecin');
            }])->where('id', $id)->get();

            $doctors = $doctors->pluck('users');
            return response()->json(['doctors' => $doctors]);
        }

       

    }

    public function listing($type)
    {
        $users = User::with('localisation')->where('role', $type)->get();
        return view('admin.pages.users.listing.index', compact('users', 'type'));
    }

    public function getProfile()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('admin.pages.users.profile', compact('user'));
    }

    public function profile(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'max:255', 'string', 'nullable'],
                'contact' => ['required', 'max:255', 'string'],
                
            ]
        )->validate();

        try{
            DB::beginTransaction();

               $user = User::where('id', Auth()->user()->id)->first();

               if($request->hasFile('file'))
               {
                  $fileName = time().'.'.$request->file->extension();
                  $request->file->move(public_path('assets/admin/uploads'), $fileName);
                  $user->profile = $fileName;
                }
               
                
                $user->name = $request->name;
                $user->surname = $request->surname;
                $user->sexe = $request->sexe;
                $user->lieuNaissance = $request->lieuNaissance;
                $user->dateNaisance = $request->dateNaisance;
                $user->profession = $request->profession;
                $user->situation = $request->situation;
                $user->save();
                DB::commit();

        }catch (\PDOException $e) {
            return $e->getMessage();
            DB::rollBack();
        }
        return back()->with('success','Vos informations ont été mises à jour.');

    }


    public function getPassword()
    {
        return view('admin.pages.users.password');
    }

    public function password(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                
                'motdepasse' => ['required', 'string', 'min:8'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        )->validate();

        if(Hash::check($request->motdepasse, Auth::user()->password))
        {
            $user = User::where('email', Auth()->user()->email)->get();
            $user->password = Hash::make($request->password);
            return back()->with('success','Votre mot de passe a été mis à jour.');
        }

        return back()->with('error','Impossible de verifier votre mit de passe!!');

        
    }

    public function role()
    {
        $users = User::role('admin')->get(); 
        dd($users);
    }

    public function dossierMedical(Request $request)
    {
        $user = User::with('localisation')->where('id', Auth::user()->id)->where('role', 'patient')->first();
        
        /* forfait et services du patient */
        $forfait = \App\Models\Forfait::where('id', Auth::user()->forfait_id)->with('services')->first();

        /* autres forfaits et services du patient */
        $autre_forfaits = \App\Models\Forfait::where('id', '<>', Auth::user()->forfait_id)->with('services')->get();

        /* liste des interventions subis pâr le patients */
        $rdvs = \App\Models\Rdv::with('type_soin', 'doctor', 'nurse', 'service')
                 ->where('user_id', Auth::user()->id)
                 ->where('state', true)
                 ->where('consult', true)
                 ->get();

        /* on recupere les antecedents medicaux du patient */

        $antecedents = \App\Models\TypeAntecedent::with(['antecedents' => function($req){
            $req->where('user_id',  Auth::user()->id);
        }])->get();

        /* on recupere les elements de biometrie de la derniere intervention du patient */
        $biometries = \App\Models\Rdv::where('user_id', Auth::user()->id)
                 ->where('state', true)
                 ->where('consult', true)
                 ->orderBy('created_at', 'DESC')
                 ->first();

        return view('admin.pages.users.medical.index', compact('user', 'rdvs', 'forfait', 'autre_forfaits', 'antecedents', 'biometries'));
    }

    public function nursePatient()
    {
        $doctor = User::findOrFail(Auth::user()->id);
        $users = User::with('localisation', 'forfait')
                 ->where('user_id', $doctor->user_id)
                 ->where('role','patient')
                 ->orderBy('name')
                 ->get();

        return view('admin.pages.users.nurses.index', compact('users'));
    }

    public function createPatient()
    {
        $user = new User();
        return view('admin.pages.users.nurses.create', compact('user'));
    }

    public function nurseStore(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255','nullable'],
                'contact' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        )->validate();

         try{
             DB::beginTransaction();

                //$doctor = User::findOrFail(Auth::user()->user_id);

                $user = User::create(["name" => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password),
                "surname" => $request->surname,"contact" => $request->contact,"localisation_id" => Auth::user()->localisation_id,
                 "role" => $request->rule,  'email_verified_at' => now(), "busy" => true, "user_id" => Auth::user()->user_id ]); 
            

                // attribution du role et envoi du mail
                $role = Role::where('name', $request->role)->first();
                $user->assignRole($role->id);
                Mail::to($user->email)->queue(new UserMail($user));
             DB::commit();

           }catch (\PDOException $e) {
               return $e->getMessage();
               DB::rollBack();
           }
       
           return redirect()->route('users.nurses.index')->with('store');


    }

    public function userForfait()
    {
        $forfait = \App\Models\Forfait::all();

        return view('admin.pages.users.subscribe', compact('forfait')); 
    }

    public function forfait(Request $request)
    { 
        \Stripe\Stripe::setApiKey ( 'sk_test_51IWNxtKEfvkV8aGd8CZDqrFF8n3gFe9n9DhN6BSE9WU8wlwsuHD4CTyHpbFWLPYej7SLShhzas8o3uf9XzInHhep00Pu5vO9Ho' );
        try {
            \Stripe\Charge::create ( array (
                "amount" => $request->input('prix')  ,
                "currency" => "usd",
                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
                "description" => "Test payment." 
            ) );
            $user = User::findOrFail(Auth::user()->id);
            $user->forfait_id = $request->forfait;
            $user->save();
            Session::flash ( 'success-message', 'Paiement effectue avec succes !' );
            return Redirect::back ();
        } catch ( \Exception $e ) {
            Session::flash ( 'fail-message', "Erreur! Essayez encore." );
            return Redirect::back ();
        } 

      
        
    }

    /* recuperons les patients d'un medecin */

    public function doctorPatient()
    {
        $users = User::where('user_id', Auth::user()->id)
                    ->where('role', 'patient')
                    ->orderBy('name')
                    ->get();
        return view('admin.pages.users.patients.index', compact('users'));
    }

    public function dossierMedicalBis($id)
    {
        $user = User::with('localisation')->where('id', $id)->where('role', 'patient')->first();
        
        /* forfait et services du patient */
        $forfait = \App\Models\Forfait::where('id', $user->forfait_id)->with('services')->first();
        
        /* autres forfaits et services du patient */
        $autre_forfaits = \App\Models\Forfait::where('id', '<>', $user->forfait_id)->with('services')->get();
        /* liste des interventions subis pâr le patients */
        $rdvs = \App\Models\Rdv::with('type_soin', 'doctor', 'nurse', 'service')
                 ->where('user_id', $id)
                 ->where('state', true)
                 ->where('consult', true)
                 ->get();

        /* on recupere les antecedents medicaux du patient */

        $antecedents = \App\Models\TypeAntecedent::with(['antecedents' => function($req) use ($id) {
            $req->where('user_id',  $id);
        }])->get();

        /* on recupere les elements de biometrie de la derniere intervention du patient */
        $biometries = \App\Models\Rdv::where('user_id', $id)
                 ->where('state', true)
                 ->where('consult', true)
                 ->orderBy('created_at', 'DESC')
                 ->first();

        return view('admin.pages.users.medical.medical.index', compact('user', 'rdvs', 'forfait', 'autre_forfaits', 'antecedents', 'biometries'));
    }


    /* affichage du formulaire d'attribtion de patients */

    public function getAssignForm()
    {
        $doctors = User::where('role','medecin')->get();
        return view('admin.pages.users.assign', compact('doctors'));
                    
    }

    /* attribution des patients à un mesdecin */ 

    public function assignPatientToDoctor(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'patient' => ['required', 'array'],  
            ]
        )->validate();


        foreach ($request->patient as  $id) {

            $user = User::findOrFail($id);
            $user->user_id = $request->doctor;
            $user->save();
            
         }

         return back()->with('success', 'operation effectuée avec succés');
    }

    /* recherche des patients ayant la meme zone que le medecin */

    public function assignFind($id)
    {
        $user = User::findOrFail($id);
        $patients = User::where([['localisation_id', $user->localisation_id], ['role', 'patient']])->get();

        return response()->json(['patients' => $patients]);
        
    }

}
