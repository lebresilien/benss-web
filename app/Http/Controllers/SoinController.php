<?php

namespace App\Http\Controllers;

use App\Models\Soin;
use App\Models\Rdv;
use App\Models\Intervention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soins = Soin::with('rdv', 'user')->get();
        return view('admin.pages.soins.index', compact('rdv','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $soin = new Soin();
        return view('admin.pages.soins.create', compact('soins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            
            try{

            DB::beginTransaction();
            $rdv = Rdv::findOrFail($request->key);
            if(!$rdv)
            return response()->json(['errors' => 'une erreur inattendue est survenue']);

            $rdv->update(["observation" => $request->observation, "ordonnance" => $request->ordonnance,"taille" => $request->taille, 
                                "consult" => true,"poids" => $request->poids,"temperature" => $request->temp]);

            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }
          return response()->json(['success' => 'succes de l\' operation']);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Soin  $soin
     * @return \Illuminate\Http\Response
     */
    public function show(Soin $soin)
    {
        try{

            DB::beginTransaction();
               $rdv = Rdv::create(["treatment" => $request->treatment, 'user_id' => $request->user_id, 'rdv_id' => $request->rdv_id]); 
            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }
        
        return redirect()->route('soins.index')->with('store');
    }

    public function details($id)
    {
        $rdv = Rdv::where('id', $id)
                   ->with('user','nurse', 'doctor', 'service')
                   ->first();

        return view('admin.pages.interventions.details', compact('rdv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Soin  $soin
     * @return \Illuminate\Http\Response
     */
    public function edit(Soin $soin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Soin  $soin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Soin $soin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Soin  $soin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $soin = findOrFail($id);
        $soin->delete();
        return redirect()->route('soins.index')->with('delete');
    }
}
