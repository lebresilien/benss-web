<?php

namespace App\Http\Controllers;

use App\Models\TypeRdv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TypeRdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeRdv::all();
        return view('admin.pages.type_rdvs.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new TypeRdv();
        return view('admin.pages.type_rdvs.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_rdvs'],
            ]
        )->validate();

        $type = TypeRdv::create(["name" => $request->name]);
        return redirect()->route('type-rdvs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeRdv  $typeRdv
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeRdv  $typeRdv
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = TypeRdv::findOrFail($id);
        return view('admin.pages.type_rdvs.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeRdv  $typeRdv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = TypeRdv::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_rdvs'],
            ]
        )->validate();

        $type->update(["name" => $request->name]);
        return redirect()->route('type-rdvs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeRdv  $typeRdv
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = typeRdv::findOrFail($id);
        $type->delete();
        return redirect()->route('type-rdvs.index');
    }
}
