<?php

namespace App\Http\Controllers;

use App\Models\Forfait;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ForfaitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forfaits = Forfait::all();
        return view('admin.pages.forfaits.index', compact('forfaits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forfait = new Forfait();
        $services = Service::all();
        return view('admin.pages.forfaits.create', compact('forfait', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:forfaits'],
                'description' => ['required', 'string', 'nullable'],
                'price' => ['regex:/^[0-9]+(\.[0-9][0-9]?)?$/'],
                'nbre_visite' => ['required', 'integer', 'min:1'],
                
            ]
        )->validate();

        try{
            DB::beginTransaction();

            $forfait = Forfait::create(["name" => $request->name, "description" => $request->description,
                                        "price" => $request->price, "nbre_visite" => $request->nbre_visite ]);
            
            foreach($request->services as $service)
            {
               $forfait->services()->attach($service);
            }

            DB::commit();

        }catch (\PDOException $e) {
            return $e->getMessage();
            DB::rollBack();
        }

        return redirect()->route('forfaits.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forfait  $forfait
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $forfait = Forfait::where('id',$id)
                            ->with('services')
                            ->first();
        
       return view('admin.pages.forfaits.show', compact('forfait'));

      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forfait  $forfait
     * @return \Illuminate\Http\Response
     */
    public function edit(Forfait $forfait)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Forfait  $forfait
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forfait $forfait)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forfait  $forfait
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forfait = Forfait::findOrFail($id);

        $forfait_services = \App\Models\ForfaitService::where('forfait_id', $forfait->id)->get();
        foreach ($forfait_services as $forfait_service )
        {
           $forfait->services()->detach($forfait_service->service_id); 
        }

        $forfait->delete();

        return redirect()->route('forfaits.index'); 

    }

    public function detailsForfait($id)
    {
        $forfait = Forfait::where('id',$id)
                           ->with('services')
                           ->first();
        return response()->json(['forfait' => $forfait]);
    }
}
