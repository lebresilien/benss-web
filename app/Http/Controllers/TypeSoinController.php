<?php

namespace App\Http\Controllers;

use App\Models\TypeSoin;
use App\Models\TypeDoctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TypeSoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeSoin::with('type_doctor')->get();
        return view('admin.pages.type_soins.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new TypeSoin();
        $types = TypeDoctor::all();
        return view('admin.pages.type_soins.create', compact('type', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_soins'],
                'price' => ['required', 'numeric'],
            ]
        )->validate();

        $type = TypeSoin::create(["name" => $request->name, "price" => $request->price, "type_doctor_id" => $request->type_doctor_id]);
        return redirect()->route('type-soins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeSoin  $typeSoin
     * @return \Illuminate\Http\Response
     */
    public function show(TypeSoin $typeSoin)
    {
        //return view('admin.pages.type_soins.show', compact('typeSoin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeSoin  $typeSoin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = TypeSoin::findOrFail($id);
        $types = TypeDoctor::all();
        return view('admin.pages.type_soins.edit', compact('type', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeSoin  $typeSoin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = TypeSoin::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_soins'],
                'price' => ['required', 'numeric'],
            ]
        )->validate();

        $type->update(["name" => $request->name, "price" => $request->price, "type_doctor_id" => $request->type_doctor_id]);
        return redirect()->route('type-soins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeSoin  $typeSoin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = typeSoin::findOrFail($id->id);
        $type->delete();
        return redirect()->route('type-soins.index');
    }
}
