<?php

namespace App\Http\Controllers;

use App\Models\Rdv;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Mail\RdvMail;

class RdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rdvs = Rdv::with('type_soin', 'user', 'nurse')->get();
        
        return view('admin.pages.rdvs.index', compact('rdvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forfait_id = Auth::user()->forfait_id;
        $nurses = User::where([['user_id', Auth::user()->user_id], ['role', 'infirmier']])->get();              
        $rdv = new Rdv();
        $type_soins = \App\Models\TypeSoin::all();
        $type_interventions = \App\Models\TypeIntervention::all();
        $forfait = \App\Models\Forfait::where('id', $forfait_id)->with('services')->get();
        $services = $forfait->pluck('services')[0];
        return view('admin.pages.rdvs.create', compact('rdv', 'type_soins', 'type_interventions', 'nurses', 'services'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Validator::make(
            $request->all(),
            [
                'date_rdv' => ['required', 'date'],
                'heure_rdv' => ['required', 'date_format:H:i'],
                'symptomes' => ['string', 'max:255', 'nullable'],
            ]
        )->validate();

        if(Auth::user()->user_id == Null)
           return redirect()->back()->with('status', 'Vous ne pouvez pas effectuer cette operation car aucun medecin ne vous a été attribué');
        
        /* $doctor = \App\Models\User::where('id',Auth::user()->user_id)->where('role','medecin')->first();
        $nurse = \App\Models\User::where('user_id',$doctor->id)
                                  ->where('role','infirmier')
                                  ->where('localisation_id', Auth::user()->localisation_id)
                                  ->first(); */
        $date_rdv = $request->date_rdv . ' ' . $request->heure_rdv . ':00';
        

        //$type = \App\Models\TypeIntervention::where('name',$request->type_in)->first();
        
        try{

            DB::beginTransaction();

              /* on verfie si le âtient a un forfait ou pas */

              if(Auth::user()->forfait_id == NULL )
              {
                $rdv = Rdv::create(["symptomes" => $request->symptomes, 'user_id' => Auth::user()->id, 'date_rdv' => $date_rdv,
                'nurse_id' => $request->nurse,"type_soin_id" => 1, "service_id" => $request->type, 'doctor_id' => Auth::user()->user_id]); 
              }else
              {

                /* on compte le nombre de rendez */

                $rdvCount = Rdv::where('user_id', Auth::user()->id)
                                   ->whereYear('date_rdv', date('Y'))
                                   ->whereMonth('date_rdv', date('m'))
                                   ->count();
                $forfaitCount = \App\Models\Forfait::findOrFail(Auth::user()->forfait_id);
                
                if($rdvCount <= $forfaitCount->nbre_visite )
                {
                    $rdv = Rdv::create(["symptomes" => $request->symptomes, 'user_id' => Auth::user()->id, 'date_rdv' => $date_rdv,
                    'nurse_id' => $request->nurse,"type_soin_id" => 1,"service_id" => $request->type, 'doctor_id' => Auth::user()->user_id, 'forfait_id' => Auth::user()->forfait_id ]);
                }else
                {
                    $user = User::findOrFail(Auth::user()->id);
                    $user->forfait_id = NULL;
                    $user->save();
                    return redirect()->back()->with('status', 'Vous avez atteint la limite de votre forfait');
                }
              }  

               // envoi du mail à l'infirmier
               $nurse = User::findOrFail($request->nurse);
               Mail::to($nurse->email)->queue(new RdvMail($rdv));
            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }
        
        return redirect()->route('rdvs.patient')->with('store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function show(Rdv $rdv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rdv = Rdv::findOrFail($id);
        $user = User::findOrFail($rdv->user_id);
        $type_soins = \App\Models\TypeSoin::all();
        $type_interventions = \App\Models\TypeIntervention::all();
        $forfait = \App\Models\Forfait::where('id', $user->forfait_id)->with('services')->get();
        $services = $forfait->pluck('services')[0];
        return view('admin.pages.rdvs.edit', compact('rdv', 'type_soins', 'type_interventions', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        Validator::make(
            $request->all(),
            [
                'date_rdv' => ['required', 'date'],
                'heure_rdv' => ['required', 'date_format:H:i'],
                'symptomes' => ['nullable', 'string', 'max:255'],
            ]
        )->validate();

     //$nurse = User::where('id',Auth::user()->id)->where('role','infirmier')->first();
        $date_rdv = $request->date_rdv . ' ' . $request->heure_rdv . ':00';

        try{

            DB::beginTransaction();
               $rdv = Rdv::findOrFail($id);
               $rdv->update(["symptomes" => $request->symptomes, 'date_rdv' => $date_rdv,"service_id" => $request->type]); 

            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }

          return redirect()->route('rdvs.nurses')->with('update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rdv = Rdv::findOrFail($id);
        $rdv->delete();
        return redirect()->route('rdvs.nurses')->with('delete');
    }


    public function confirm($id)
    {
        
        //$nurse = User::where('user_id',Auth::user()->user_id)->where('role','infirmier')->first();
        $rdv = Rdv::findOrFail($id);
        $user = User::findOrFail($rdv->user_id);
        $rdv->state = true;
        $rdv->save();
        Mail::to($user->email)->queue(new RdvMail($rdv));
        return response()->json(['message' => 'succes']);  

    }

    public function nurseRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'doctor', 'service')
                 ->where('nurse_id', Auth::user()->id)
                 ->get();
        
        return view('admin.pages.rdvs.nurses.index', compact('rdvs'));
    }

    /* liste rendez vous non effectuées par l'infrilier */

    public function nurseInvalidRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'doctor', 'service')
                 ->where('nurse_id', Auth::user()->id)
                 ->where('state', false)
                 ->get();
        
        return view('admin.pages.rdvs.nurses.invalid', compact('rdvs'));
    }
   

    public function doctorRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'nurse', 'service')
                 ->where('doctor_id', Auth::user()->id)
                 ->where('state', true)
                 ->get();
        
        return view('admin.pages.rdvs.doctors.index', compact('rdvs'));
    }

    public function patientRdv()
    {
        $rdvs = Rdv::with('type_soin', 'doctor', 'nurse', 'service')
                ->where('user_id', Auth::user()->id)
                ->get();
        
        return view('admin.pages.rdvs.index', compact('rdvs'));
    }

    /* intervention non effectué pas 'infirmir */ 

    public function interventionNonNurse()
    {
        $rdvs = Rdv::with('type_soin', 'doctor', 'service')
                ->where('nurse_id', Auth::user()->id)
                ->where('state', true)
                ->get();
        
       return view('admin.pages.interventions.nurses.index', compact('rdvs'));
    }

      /* intervention non effectué du medecin */ 

      public function doctorInvalidRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'nurse', 'service')
                 ->where('doctor_id', Auth::user()->id)
                 ->where('state', true)
                 ->where('consult', false)
                 ->get();
        
        return view('admin.pages.interventions.doctors.index', compact('rdvs'));
    }

    /* // Fonction qui affiche les rendez-vous pour un patient
    public function patientRDV($id){
        $user = Auth::user()->id;
        $query = DB::table('rdvs')->select('date_rdv', 'type_soin_id', 'nurse_id', 'doctor_id')->where('user_id', $user)->get();
        return view('admin.pages.rdvs.patient', compact('query', 'user'));
    } */

    
}
