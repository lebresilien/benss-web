<?php

namespace App\Http\Controllers;

use App\Models\Localisation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LocalisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locales = Localisation::all();
        return view('admin.pages.locales.index', compact('locales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localisation = new Localisation();
        return view('admin.pages.locales.create', compact('localisation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:localisations'],
            ]
        )->validate();

        $locale = Localisation::create(["name" => $request->name]);
        return redirect()->route('locales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Localisation  $localisation
     * @return \Illuminate\Http\Response
     */
    public function show(Localisation $localisation)
    {
        //return view('admin.pages.locales.show', compact('localisation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Localisation  $localisation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $localisation = Localisation::findOrFail($id);
        return view('admin.pages.locales.edit', compact('localisation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Localisation  $localisation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $localisation = Localisation::findOrFail($id);

        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:localisations'],
            ]
        )->validate();

        $localisation->update(["name" => $request->name]);
        return redirect()->route('locales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Localisation  $localisation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locale = Localisation::findOrFail($id);
        $locale->delete();
        return redirect()->route('locales.index');
    }
}
