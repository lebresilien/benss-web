<?php

namespace App\Http\Controllers;

use App\Models\TypeDoctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TypeDoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeDoctor::all();
        return view('admin.pages.type_doctors.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new TypeDoctor();
        return view('admin.pages.type_doctors.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_doctors'],
            ]
        )->validate();

        $type = TypeDoctor::create(["name" => $request->name]);
        return redirect()->route('type-doctors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeDoctor  $typeDoctor
     * @return \Illuminate\Http\Response
     */
    public function show(TypeDoctor $typeDoctor)
    {
        //return view('admin.pages.type_doctors.show', compact('typeDoctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeDoctor  $typeDoctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = TypeDoctor::findOrFail($id);
        return view('admin.pages.type_doctors.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeDoctor  $typeDoctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $type = TypeDoctor::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_doctors'],
            ]
        )->validate();

        $type->update(["name" => $request->name]);
        return redirect()->route('type-doctors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeDoctor  $typeDoctor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = TypeDoctor::findOrFail($id);
        $type->delete();
        return redirect()->route('type-doctors.index');
    }
}
