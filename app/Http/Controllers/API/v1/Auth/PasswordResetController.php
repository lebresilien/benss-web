<?php

namespace App\Http\Controllers\API\v1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\User;
use App\PasswordReset;
use Illuminate\Support\Str;
use Validator;
use Response;

class PasswordResetController extends Controller
{
    protected $rules =
    [
        'email' => 'required|string|email'
    ];

    protected $ruled =
    [
        'email' => 'required|string|email',
        'password' => 'required|string|min:8|confirmed',
        'token' => 'required|string'
    ];

    public function create(Request $request)
    {
        $validator = Validator::make($request->all() , $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else
        {

        $user = User::where('email', $request->email)->first();

        if(empty($user))
         {
           return Response::json(array('errors' => 'Aucun utilisateur trouvé avec cette adresse email '));  
         }else
         {
            $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60)
             ]
           );

            if($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );

            return Response::json(array('success' => 'Un mail de renitialisation a été envoyé'));
         }
            

       }
    }

    public function find($token)
    {
    	$passwordReset = PasswordReset::where('token', $token)
            ->first();

        if(!$passwordReset)
            /*return Response::json(array(
                'errors' => 'Le token de validation est incorrect.'
            ));*/
            return view('pages.user.invalid-password-reset-token');

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return view('pages.user.invalid-password-reset-token');
        }        
        return view('pages.user.password-reset', ['token' => $token, 'email' => $passwordReset->email]);
    }

    public function reset(Request $request)
    {
    	$validator = Validator::make($request->all() , $this->ruled);
        if($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else
        {
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if(!$passwordReset)
            return Response::json(array(
                'errors' => 'Le token de validation est incorrect.'
            ));

        $user = User::where('email', $passwordReset->email)->first();

        if(empty($user))
            return Response::json(array(
                'errors' => 'Aucun utilisateur trouvé avec cette adresse.'
            ));    

        $user->password = bcrypt($request->password);
        $user->save();        
        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess($passwordReset));
        return Response::json(array(
                'success' => 'Votre mot de passe a été renitialisé avec succes. Veuillez vous connecter.'
            ));
     }
    }
    
}
