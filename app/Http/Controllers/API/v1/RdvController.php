<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Models\Rdv;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Mail\RdvMail;

class RdvController extends BaseController
{

     /**
     * @OA\Get(
     ** path="/api/rdvs",
     *   tags={"Rdv"},
     *   summary="rdv",
     *   operationId="rdv",
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *)
     **/
    /**
     * show form rdv api
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $forfait_id = auth('api')->user()->forfait_id;
        if(!$forfait_id) return response()->json(['message' => 'vous ne pouvez pas effectuer de rdv car vous n\'avez pas de forfait actif']);
        
        $nurses = User::where([['user_id', auth('api')->user()->user_id], ['role', 'infirmier']])->get();              
        if(!$nurses) return response()->json(['message' => 'votre forfait n\' inclut pas encore d\'infirmier ']);
        
        $forfait = \App\Models\Forfait::where('id', $forfait_id)->with('services')->get();
        $services = $forfait->pluck('services')[0];

        return response()->json([
               'nurses' => $nurses,
               'services' => $services,
            ], 
        200);
       
    }

    /**
     * @OA\Post(
     *     path="/api/rdvs",
     *     tags={"Rdv"},
     *     summary="Enregistrement d'un nouveau rdv",
     *     description="Enregistrement d'un nouveau rdv",
     *     @OA\RequestBody(
     *         description="Formulaire d'enregistrement d'un rdv",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                  @OA\Property(
     *                      property="nurse_id",
     *                      description="identifiant de l'infirmiere.",
     *                      type="integer",
     *                  ),
     *                  @OA\Property(
     *                      property="service_id",
     *                      description="Identifiant du service.",
     *                      type="integer",
     *                  ),
     *                  @OA\Property(
     *                      property="date_rdv",
     *                      description="Date et Heure du rdv",
     *                      type="Date",
     *                  ),  
     *                  @OA\Property(
     *                      property="heure_rdv",
     *                      description="Heure du rdv",
     *                      type="Time",
     *                  ), 
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */

    public function store(Request $request)
    {
        
        Validator::make(
            $request->all(),
            [
                'date_rdv' => ['required', 'date'],
                'heure_rdv' => ['required', 'date_format:H:i'],
                'symptomes' => ['string', 'max:255', 'nullable'],
            ]
        )->validate();

        if(auth('api')->user()->user_id == Null)
            return response()->json(['message' =>  'Vous ne pouvez pas effectuer cette operation car aucun medecin ne vous a été attribué']);
        
            $date_rdv = $request->date_rdv . ' ' . $request->heure_rdv . ':00';

         /* on verifie si le client a droit à ce service */

         $services = \App\Models\ForfaitService::where([['service_id', $request->service_id], ['forfait_id', auth('api')->user()->forfait_id]])->first();
         if(!$services)  return response()->json(['message' => 'Vous forfait ne prend pas en compte ce service.']);

         /* on verifie si l\'infirmier est bel en charge du patient */
         $nurse = User::where([['id', $request->nurse], ['user_id', auth('api')->user()->user_id]])->first();
         if(!$nurse)  return response()->json(['message' => 'Cet infirmier n\'est pas en charge de vous.']);
        
        try{

            DB::beginTransaction();

                /* on compte le nombre de rendez-vous*/

                $rdvCount = Rdv::where('user_id', auth('api')->user()->id)
                                   ->whereYear('date_rdv', date('Y'))
                                   ->whereMonth('date_rdv', date('m'))
                                   ->count();
                $forfaitCount = \App\Models\Forfait::findOrFail(auth('api')->user()->forfait_id);
                
                if($rdvCount <= $forfaitCount->nbre_visite )
                {
                    $rdv = Rdv::create(['user_id' => auth('api')->user()->id, 'date_rdv' => $date_rdv,
                    'nurse_id' => $request->nurse,"type_soin_id" => 1,"service_id" => $request->service_id, 'doctor_id' => auth('api')->user()->user_id, 'forfait_id' => auth('api')->user()->forfait_id ]);
                }else
                {
                    $user = User::findOrFail(auth('api')->user()->id);
                    $user->forfait_id = NULL;
                    $user->save();
                    return response()->json('message', 'Vous avez atteint la limite de votre forfait');
                } 

               // envoi du mail à l'infirmier
               $nurse = User::findOrFail($request->nurse);
               Mail::to($nurse->email)->queue(new RdvMail($rdv));
            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }
        
        //return response()->json($rdv, 201);
        return $this->sendResponse($rdv, "le rdv a été crée avec succés");
    }



        /**
     * @OA\put(
     *     path="/api/rdvs",
     *     tags={"Rdv"},
     *     summary="Modification d'un nouveau rdv",
     *     description="Modification d'un nouveau rdv",
     *     @OA\Parameter(
     *          name="id",
     *          description="recupération de l'ID du rdv",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         description="Formulaire d'Modification d'un rdv",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                  @OA\Property(
     *                      property="heure_rdv",
     *                      description="heure du rdv.",
     *                      type="Time",
     *                  ),
     *                  @OA\Property(
     *                      property="service_id",
     *                      description="Identifiant du service.",
     *                      type="integer",
     *                  ),
     *                  @OA\Property(
     *                      property="date_rdv",
     *                      description="Date du rdv",
     *                      type="Date",
     *                  ),  
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */
    public function update(Request $request,  $id)
    {
        Validator::make(
            $request->all(),
            [
                'date_rdv' => ['required', 'date'],
                'heure_rdv' => ['required', 'date_format:H:i'],
                'symptomes' => ['nullable', 'string', 'max:255'],
            ]
        )->validate();

        $rdv = Rdv::findOrFail($id);

         $this->authorize('update', $rdv);

         /* on verifie si le client a droit à ce service */

         $services = \App\Models\ForfaitService::where([['service_id', $request->service_id], ['forfait_id', auth('api')->user()->forfait_id]])->first();
         if(!$services)  return response()->json(['message' => 'Vous forfait ne prend pas en compte ce service.']);

        $date_rdv = $request->date_rdv . ' ' . $request->heure_rdv . ':00';

        try{

            DB::beginTransaction();
               $rdv = Rdv::findOrFail($id);
               $rdv->update(['date_rdv' => $date_rdv,"service_id" => $request->service_id]); 

            DB::commit();

          }catch (\PDOException $e) {
              return $e->getMessage();
              DB::rollBack();
          }

          return $this->sendResponse($rdv, "Le rdv a été modifié avec succés");
    }

    
    /**
     * @OA\get(
     *     path="/api/rdvs/{id}/confirm",
     *     tags={"Rdv"},
     *     summary="Confirmation d'un nouveau rdv",
     *     description="Confirmation d'un nouveau rdv",
     *     @OA\Parameter(
     *          name="id",
     *          description="recupération de l'ID du rdv",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */
    public function confirm($id)
    {
        $rdv = Rdv::findOrFail($id);
        $this->authorize('update', $rdv);

        $user = User::findOrFail($rdv->user_id);
        $rdv->state = true;
        $rdv->save();
        Mail::to($user->email)->queue(new RdvMail($rdv));
        return $this->sendResponse($rdv, "Le rdv a été modifié avec succés, un mail a été enviyé au patient"); 

    }

    /**
     * @OA\get(
     *     path="/api/rdvs/nurse",
     *     tags={"Rdv"},
     *     summary="Liste des rdvs d'un infirmier",
     *     description="Liste des rdvs d'un infirmier",
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */

    public function nurseRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'doctor', 'service')
                 ->where('nurse_id', auth('api')->user()->id)
                 ->get();
        return $this->sendResponse($rdvs, "Liste des rdvs de l'infirmier ");
    }

    /**
     * @OA\get(
     *     path="/api/rdvs/nurse/invalid",
     *     tags={"Rdv"},
     *     summary="Liste des rdvs d'un infirmier non validés",
     *     description="Liste des rdvs d'un infirmier non validés",
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */

    public function nurseInvalidRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'doctor', 'service')
                 ->where('nurse_id', auth('api')->user()->id)
                 ->where('state', false)
                 ->get();
        
        return $this->sendResponse($rdvs, "Liste des rdvs non validés de l'infirmier ");
    }


     /**
     * @OA\get(
     *     path="/api/rdvs/patient",
     *     tags={"Rdv"},
     *     summary="Liste des rdvs d'un patient",
     *     description="Liste des rdvs d'un  patient",
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */
    public function patientRdv()
    {
        $rdvs = Rdv::with('type_soin', 'doctor', 'nurse', 'service')
                ->where('user_id', auth('api')->user()->id)
                ->get();
        
        return $this->sendResponse($rdvs, "Liste des rdvs d\'un patient");
    }


     /**
     * @OA\get(
     *     path="/api/rdvs/nurse/invalid",
     *     tags={"Rdv"},
     *     summary="Liste des rdvs d'un medecin non validés",
     *     description="Liste des rdvs d'un medecin non validés",
     *     @OA\Response(
     *         response=200,
     *         description="succsess response"
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="unexpected error"
     *     )
     * )
     */
    public function doctorInvalidRdv(){

        $rdvs = Rdv::with('type_soin', 'user', 'nurse', 'service')
                 ->where('doctor_id', Auth::user()->id)
                 ->where('state', true)
                 ->where('consult', false)
                 ->get();
        
        return $this->sendResponse($rdvs, "Liste des rdvs non validés par un medecin");
    }


}
