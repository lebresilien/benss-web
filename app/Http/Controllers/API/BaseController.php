<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    use Helpers;

    public function sendResponse($result, $message)
    {
        if (isset($result['data'])) {
            $response = [
                'success' => true,
                'message' => $message
            ];
            $response = array_merge($result, $response);
        } else {
            $response = [
                'success' => true,
                'data' => $result,
                'message' => $message
            ];
        }
        return response()->json($response, 200);
    }
    /**
     * return error response.
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
