<?php

namespace App\Http\Controllers;

use App\Models\Antecedent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AntecedentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $antecedents = Antecedent::with('type_antecedent')->where('user_id', Auth::user()->id)->get();
        return view('admin.pages.antecedents.index', compact('antecedents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $antecedent = new Antecedent();
        $type_antecedent = new \App\Models\TypeAntecedent();
        return view('admin.pages.antecedents.create', compact('antecedent', 'type_antecedent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:antecedents'],  
            ]
        )->validate();

        try{
            DB::beginTransaction();

            $antecedent = Antecedent::create(["name" => $request->name, "type_antecedent_id" => $request->type_antecedent_id,
                                        "user_id" => Auth::user()->id]);
            DB::commit();

        }catch (\PDOException $e) {
            return $e->getMessage();
            DB::rollBack();
        }

       return redirect()->route('antecedents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $antecedent = Antecedent::findOrFail($id);
        $type_antecedent = new \App\Models\TypeAntecedent();
        return view('admin.pages.antecedents.edit', compact('antecedent', 'type_antecedent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $antecedent = Antecedent::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:antecedents'],
            ]
        )->validate();

        $antecedent->update(["name" => $request->name, "type_antecedent_id" => $request->type_antecedent_id]);
        return redirect()->route('antecedents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $antecedent = Antecedent::findOrFail($id);
        $antecedent->delete();
        return redirect()->route('antecedents.index');
    }
}
