<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\TypeAntecedent;

class TypeAntecedentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeAntecedent::all();
        return view('admin.pages.type_antecedents.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new TypeAntecedent();
        return view('admin.pages.type_antecedents.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_antecedents'],
            ]
        )->validate();

        $type = TypeAntecedent::create(["name" => $request->name]);
        return redirect()->route('type-antecedents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = TypeAntecedent::findOrFail($id);
        return view('admin.pages.type_rdvs.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = TypeAntecedent::findOrFail($id);
        Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255', 'unique:type_antecedents'],
            ]
        )->validate();

        $type->update(["name" => $request->name]);
        return redirect()->route('type-antecedents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = TypeAntecedent::findOrFail($id);
        $type->delete();
        return redirect()->route('type-antecedent.index');
    }
}
