<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Notifications\RegisteredNotification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CONFIRM;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surnname' => ['required', 'string', 'max:255','sometimes'],
            'contact' => ['required', 'string', 'max:255'],
            'profession' => ['required', 'string', 'max:255', 'sometimes'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'localisation_id' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'profession' => $data['profession'],
            'contact' => $data['contact'],
            'localisation_id' => $data['localisation_id'],
            'email' => $data['email'],
            'role' => 'patient',
            'password' => Hash::make($data['password']),
        ]);

        $role = Role::where('name', 'patient')->first();
        $user->assignRole($role);

        $users = User::where('role', 'admin')->get();

        foreach ($users as $userr) {
            $userr->notify(new RegisteredNotification($user));
        }

        

        return $user;
    }
}
