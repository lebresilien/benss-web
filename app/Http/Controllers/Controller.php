<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(
 *   version="1.0.0",
 *   title="Documentation des services Web Benss Cameroun",
 *   description="Documentation pour le projet de développement de services Web."
 *      @OA\Contact(
 *          email="admin@bensscameroun.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 * 
 * @OA\SecurityScheme(
 *   type="oauth2",
 *   securityScheme="Oauth2Password",
 *   name="Password Based",
 *   scheme="bearer",
 *   description="Utilisez client_id / client_secret et 
 *                votre courriel / mot de passe pour 
 *                obtenir un jeton d'authentification.",
 *   in="header",
 *   @OA\Flow(
 *     flow="password",
 *     authorizationUrl="/oauth/authorize",
 *     tokenUrl="/oauth/token",
 *     refreshUrl="/oauth/token/refresh",
 *     scopes={}
 *   )
 * )
 *   @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 *   )
 *
 *   @OA\Tag(
 *     name="Projects",
 *     description="API Endpoints of Projects"
 *    )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
